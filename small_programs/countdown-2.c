// to make it work for times bigger than 60 minutes this has to be extended

#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  int countdown_min_total;
  if (argc == 2) {
    countdown_min_total = atoi(argv[1]);
  } else {
    puts ("Define your countdown length in minutes!");
    scanf ("%d", &countdown_min_total);
  }

  int countdown_sec_total = countdown_min_total * 60;

  int countdown_sec;
  int countdown_min;

  write(1, "\033c", 3);

  while (countdown_sec_total >= 0) {
    if (countdown_sec_total >= 60) {
      countdown_sec = countdown_sec_total % 60;
      countdown_min = (countdown_sec_total - countdown_sec) / 60;
    } else {
      countdown_sec = countdown_sec_total;
      countdown_min = 0;
    }

    write(1, "\033c", 3);

    printf ("%02d:%02d", countdown_min, countdown_sec);
    fflush(stdout);
    printf ("\r");
    
    sleep(1);
    countdown_sec_total--;
  }

  system("paplay /usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga");
  printf ("THE TIME IS UP!\n");

  return 0;
}
