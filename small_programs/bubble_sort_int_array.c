// this program sorts dates that are stored as an integer array
// it uses a bubble sort algorithm

#include <stdio.h>

void bubble_sort_int_array(int dates[], size_t num_of_elems) {
  int exchanged_elems = 1;

  while (exchanged_elems) {
    exchanged_elems = 0;
    for (int i = 0; i < (num_of_elems-1); i++) {
      if (dates[i] > dates[i+1]) {
        int temp_var = dates[i];
        dates[i] = dates[i+1];
        dates[i+1] = temp_var;
        exchanged_elems = 1;
      }
    }
  }
}

int main() {
  int my_dates[10];
  my_dates[0] = 20230404;
  my_dates[1] = 20240404;
  my_dates[2] = 20220204;
  my_dates[3] = 20210306;
  my_dates[4] = 20230916;
  my_dates[5] = 20231026;
  my_dates[6] = 20240101;
  my_dates[7] = 20220101;
  my_dates[8] = 20231111;
  my_dates[9] = 20181111;

  bubble_sort_int_array(my_dates, 10);

  for (int i = 0; i<10; i++) {
    printf("element %d: %d\n", i, my_dates[i]);
  }

  return 0;
}
