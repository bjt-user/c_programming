#include <stdio.h>
#include <string.h>
#include <limits.h>

// it still unclear what to do if a line is longer than LINE_MAX

int main(int argc, char **argv)
{
        if (argc < 2) {
                printf ("Please provide a file to check!\n");
                return 1;
        }

        printf("Maximum line length according to limits.h: %d\n", LINE_MAX);

        char *file_name = argv[1];
        FILE *file = fopen(file_name, "r");
        if (file == NULL) {
                perror("Error opening file");
                return 1;
        }

        char line[LINE_MAX];
        while (fgets(line, sizeof(line), file) != NULL) {
                int length = strlen(line);
                printf("Length of this line: %d\n", length);

                // Check if the line ends with CRLF
                if (length >= 2 && line[length - 2] == '\r'
                    && line[length - 1] == '\n') {
                        printf("Windows (CRLF) line ending found.\n");
                } else {
                        printf("Linux (LF) line ending found.\n");
                }
        }

        fclose(file);
        return 0;
}
