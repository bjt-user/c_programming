// cut a string into two parts by the first occurence of delimiter
// and choose the first part (side 0) or the second part (side 1)
// the chosen part will overwrite the original string

// cut a string into two parts by delimiter
// and choose the first part (side 0) or the second part (side 1)
// the chosen part will overwrite the original string

#include <string.h>

void cut_string(char my_string[], char delimiter, int side) {
	char part1[256] = "";
	char part2[256] = "";

	int split = 0;

	int j = 0;
	for (int i = 0; i < strlen(my_string); i++) {
		if (my_string[i] == delimiter) {
			if (split == 0) {
				split = 1;
				continue;
			}
		}

		if (split == 0) {
			part1[i] = my_string[i];
		} else {
			part2[j] = my_string[i];
			j++;
		}
	}

	memset(my_string, '\0', strlen(my_string));
	if (side == 0) {
		strcpy(my_string, part1);
	} else {
		strcpy(my_string, part2);
	}
}
