// install libpulse-dev or libpulse package depending on your distro
// compile: gcc -Wall play_raw_audio_file.c -lpulse-simple

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <pulse/simple.h>

int main() {
    char content[1024];

    int myfd = open("alarm.wav", O_RDONLY);

    pa_simple* simple = NULL;
    pa_sample_spec ss;
    ss.format = PA_SAMPLE_S16LE;
    ss.rate = 48000;
    ss.channels = 2;

    simple = pa_simple_new(NULL, "Audio Playback", PA_STREAM_PLAYBACK, NULL, "playback", &ss, NULL, NULL, NULL);

    int i = 0;
    while (read(myfd, content, sizeof(content))) {
      // skip the first 1024 bytes to not play the file header
      if (i != 0) {
        pa_simple_write(simple, content, sizeof(content), NULL);
      }
      i++;
    }

    pa_simple_drain(simple, NULL);

    pa_simple_free(simple);

    close(myfd);

    return 0;
}
