// WARNING: this function is very slow when searching in big files
// if it can't find a string in a big file it will use a lot of memory and cpu (at least in wsl)

// this function will seek for a string in an open file
// it will place the file position AFTER that string

// performance: since this search works by searching one char at a time it is very efficient when the search string
// is very close to the current file position but very unefficient when the search string is very far
// away from the current file position or when it is not found and the file is very large
// 3.3s for a file with 92843 lines

#include <fcntl.h>
#include <unistd.h>
#include <string.h>

void seek_string_a(int fd, char search_string[]) {
  int j = 0;
  char char_reader = '\0';

  while(read(fd, &char_reader, 1)) {
    if (char_reader == search_string[j]) {
      j++;
    } else {
      j = 0;
    }
    if (j == (strlen(search_string))) {
      break;
    }
  }
}
