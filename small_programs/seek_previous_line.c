#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

void seek_previous_line(int fd);
void seek_line_start(int fd);

void seek_previous_line(int fd) {
  seek_line_start(fd);
  lseek(fd, -1, SEEK_CUR);
  seek_line_start(fd);
}

// set file position to the beginning of the line
void seek_line_start(int fd) {
  char cur_char = '\0';

  while (cur_char != '\n') {
    int ret_lseek = lseek(fd, -1, SEEK_CUR);
    if (ret_lseek == -1) {
      break;
    }
    read(fd, &cur_char, 1);
    if (cur_char != '\n')
      lseek(fd, -1, SEEK_CUR);
  }
}

int main() {
  int myfd = open("myfile.txt", O_RDONLY);
  char my_char = '\0';

  if (myfd == -1)
    printf ("Opening the file failed.\n");

  lseek(myfd, -1, SEEK_END);

  seek_line_start(myfd);

  read(myfd, &my_char, 1);
  printf ("my_char: %c\n", my_char);

  seek_previous_line(myfd);
  seek_previous_line(myfd);
  seek_previous_line(myfd);
  seek_previous_line(myfd);
  seek_previous_line(myfd);
  seek_previous_line(myfd);

  read(myfd, &my_char, 1);
  printf ("my_char: %c\n", my_char);

  return 0;
}
