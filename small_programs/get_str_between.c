// get the substring from a string called buffer that is between the strings str1 and str2
// the caller needs to free the returned string

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char *get_str_between(char *buffer, char *str1, char *str2) {
        int j = 0;
        int k = 0;
        int l = 0;
        int found1 = 0;
        int found2 = 0;
        char *result_buffer = malloc(2048);
        memset(result_buffer, '\0', 2048);

        for (int i = 0; i < strlen(buffer); i++) {
                if (j == strlen(str1)) {
                        found1 = 1;
                }
                if (k == strlen(str2)) {
                        found2 = 1;
                }
                if (buffer[i] == str1[j] && found1 == 0) {
                        j++;
                }
                if (buffer[i] == str2[k] && found2 == 0) {
                        k++;
                }
                if (found1) {
                        if (found2) {
                                // replace str2 in the result sring with null terminators
                                for (int i = strlen(str2); i != 0; i--) {
                                        result_buffer[l-strlen(str2)+i-1] = '\0';
                                }
                                break;
                        }
                        result_buffer[l] = buffer[i];
                        l++;
                }
        }

        return result_buffer;
}
