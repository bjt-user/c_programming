//usr/bin/gcc -Wall "$0" && exec ./a.out "$@"

// The first time getpwent() is called, it returns the first entry
// thereafter, it returns successive entries.
// the pw_passwd field is always 'x' though...

#include <stdio.h>
#include <sys/types.h>
#include <pwd.h>

// struct passwd is NOT a linked list!

int main() {
        struct passwd* mypass;

        while ((mypass = getpwent()) != NULL) {
                printf ("mypass.pw_name: %s\n", mypass->pw_name);
                printf ("mypass.pw_passwd: %s\n", mypass->pw_passwd);
                printf ("mypass.pw_uid: %d\n", mypass->pw_uid);
        }

        return 0;
}
