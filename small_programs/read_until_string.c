// this program reads from a text file into a string until it finds a specific string
// it also reads that "search" string into the buffer, but then truncates it by placing
// the null character between search string and the buffer string

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

void read_until_string(int fd, char buffer[], char search_string[]) {
  int i = 0;
  int j = 0;

  while(read(fd, &buffer[i], 1)) {
    if (buffer[i] == search_string[j]) {
      j++;
    } else {
      j = 0;
    }
    if (j == (strlen(search_string))) {
      printf ("String found.\n");
      break;
    }
    i++;
  }
  buffer[strlen(buffer)-strlen(search_string)] = '\0';
}

int main() {
  int myfd = open("calendar.ics", O_RDONLY);
  char my_buffer[4096] = "";

  char ss[] = "PRODID";

  read_until_string(myfd, my_buffer, ss);

  printf ("%s\n", my_buffer);

  close(myfd);

  return 0;
}
