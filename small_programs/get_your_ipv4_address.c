#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <sys/socket.h>

int main() {
  struct ifaddrs *my_ifaddrs;

  getifaddrs(&my_ifaddrs);

  struct ifaddrs *my_address = my_ifaddrs;

  while(my_address) {
    int family = my_address->ifa_addr->sa_family;
    char interface_name[100];
    //char interface_name[100] = my_address->ifa_name;
    strcpy(interface_name, my_address->ifa_name);
    // AF_INET has the value 2
    // AF_INET means IPv4 (opposed to AF_INET6)
    if (family == AF_INET) {
      printf("%s\n", interface_name);
      char data[100];
      getnameinfo(my_address->ifa_addr, 100, data, sizeof(data), \
      0, 0, NI_NUMERICHOST);

      printf ("%s\n\n", data);
    }
    my_address = my_address->ifa_next;
  }
  
  return 0;
}
