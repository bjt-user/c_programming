// make sure ncurses is installed: sudo pacman -S ncurses
// compile with: gcc -Wall -lncurses ncurses-menu.c

#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

int main() {
    // 2d array for 3 choices with a maximum of 10 characters each
    char choices[3][10] = {"Orange", "Apple", "Banana"};
    int choice;
    int highlight = 0;

    initscr();
    clear();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    curs_set(0); // Hide the cursor

    while (1) {
        clear();

        for (int i = 0; i < 3; i++) {
            if (i == highlight)
                attron(A_REVERSE);

            mvprintw(i + 1, 1, choices[i]);

            if (i == highlight)
                attroff(A_REVERSE);
        }

        choice = getch();

        switch (choice) {
            case KEY_UP:
            case 'k':
                highlight--;
                if (highlight < 0)
                    highlight = 0;
                break;
            case KEY_DOWN:
            case 'j':
                highlight++;
                if (highlight > 2)
                    highlight = 2;
                break;
            case 10:
                // Enter key pressed
                clear();
                mvprintw(5, 10, "Selected: %s", choices[highlight]);
                refresh();
                getch();
                endwin();
                exit(0);
                break;
            default:
                break;
        }
    }

    endwin();
    return 0;
}

