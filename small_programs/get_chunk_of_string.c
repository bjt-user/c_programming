// this function will remove everything from the beginning of a string until the X occurence of delimiter you specify
// if the delimiter does not occur or does not occur as often as specified, raw_string will be empty after calling this function 
void get_chunk_of_string(char raw_string[], char delimiter, int occurence) {
	int chunk = 0;
	int j = 0;
	char *tmp_string = malloc(strlen(raw_string));
	memset(tmp_string, '\0', strlen(raw_string));

	for (int i = 0; i < strlen(raw_string); i++) {
		if (raw_string[i] == delimiter) {
			chunk++;
		}
		if (chunk == occurence) {
			tmp_string[j] = raw_string[i];
			j++;
		}
	}
	strcpy(raw_string, tmp_string);
	free(tmp_string);
}
