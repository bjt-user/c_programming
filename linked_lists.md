Warning: Using linked list requires an incredible amount of intelligence.\
If you want to go insane, stick to fixed size arrays and arrays of structs.\
K&R doesn't even cover linked lists.\
Getting data out of linked lists (i.e. in network programming) can be done with the help of gdb, books and boiler plate code from chatgpt.

http://cslibrary.stanford.edu/103/

http://cslibrary.stanford.edu/105/LinkedListProblems.pdf

https://www.geeksforgeeks.org/data-structures/linked-list/

There are different types of linked lists.\
singly linked list: you can only go forward with one pointer to the next node\
doubly linked list: you can also go backwards with one pointer to the next and one pointer to the previous node

> A linked list is represented by a pointer to the first node of the linked list.\
The first node is called the head of the linked list.\
If the linked list is empty, then the value of the head points to NULL.

The **last node** of the list should always point to `NULL`.\
https://stackoverflow.com/questions/31913059/linked-lists-in-c-last-node-point-to-null \
Sometimes the linked list will work even though you do not make the last node point to NULL, because there is an uninitialized pointer to NULL, but that is by luck and is unreliable.

Looks like in C structs will be used to create "nodes".

#### simple example with functions to count, print and push a node to the beginning of the list

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct event {
  char summary[100];
  int date;
  struct event *next;
};

int count_list_length(struct event *head) {
  struct event *current = head;
  int list_length = 0;

  while (current != NULL) {
    list_length++;
    current = current->next;
  }
  return list_length;
}

void print_list(struct event *my_head) {
  struct event *current = my_head;
  while (current != NULL) {
    printf("%s\n", current->summary);
    printf("%d\n", current->date);
    current = current->next;
  }
}

void push(struct event **head_ref, int data, char new_summary[]) {
  struct event* newNode = malloc(sizeof(struct event));
  newNode->date = data;
  strcpy(newNode->summary, new_summary);
  newNode->next = *head_ref;
  *head_ref = newNode;
}

int main() {
  // initialize an empty list
  struct event *head = NULL;

  print_list(head);

  push(&head, 125, "eat bread");
  print_list(head);
  printf ("length of the list: %d\n", count_list_length(head));

  push(&head, 13, "take exam and drink tea");
  print_list(head);
  printf ("length of the list: %d\n", count_list_length(head));

  return 0;
}
```

***

#### push new node to the front of the list

```
void push(struct person **my_head, int id, char name[]) {
	struct person* new_node = malloc(sizeof(struct person));
	new_node->id = id;
	strcpy(new_node->name, name);
	new_node->next = *my_head;
	*my_head = new_node;
}
```

#### printing the list

```
void print_list(struct person *my_node) {
  while (my_node != NULL) {
    printf("%d\n", my_node->id);
    printf("%s\n", my_node->name);
    my_node = my_node->next;
  }
}
```

***

#### appending to the list
This worked for appending a node to the end of the list:
```
void append(struct event **head, int data) {
  if (*head == NULL) {
    printf ("Can't append to an empty list, so it will be pushed.\n");
    push(head, data);
    return;
  }
  struct event *current = *head;
  struct event *new_node = malloc(sizeof(struct event));
  new_node->date = data;
  while (current != NULL) {
    if (current->next == NULL) {
      current->next = new_node;
      new_node->next = NULL;
    }
    current = current->next;
  }
}
```
Call the function like this:
```
append(&head, 20220101);
```

***

#### get specific node of a list

Get a specific node of a list by number (the list starts with 0)
```
struct person *get_node(struct person *my_head, int node_number) {
	if (my_head == NULL) {
		printf ("Can't get node because list is empty.\n");
		return my_head;
	}
	int i = 0;

	while (my_head != NULL) {
		if (i == node_number)
			break;
		
		i++;
		my_head = my_head->next;
	}

	return my_head;
}
```
to get the third node of a list:
```
struct person *two;
two = get_node(head, 2);
```

#### sorted insert

A slightly modified version of the Stanford code.\
It inserts a node at the correct position. (sort criteria here is `data`)\
That way you never really have to sort a list, because you will always insert a new node at the correct position.

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person {
	int data;
	char name[256];
	struct person *next;
};

void sorted_insert(struct person** head, int data, char name[]) {
	// Special case for the head end
	struct person *new_node = malloc(sizeof(struct person));
	(*new_node).data = data;
	strcpy((*new_node).name, name);

	if (*head == NULL || (*head)->data >= new_node->data) {
		new_node->next = *head;
		*head = new_node;
	}
	else {
		// Locate the node before the point of insertion
		struct person* current = *head;
		while (current->next!=NULL && current->next->data<new_node->data) {
		current = current->next;
	}
		new_node->next = current->next;
		current->next = new_node;
	}
}

void print_list(struct person *my_node) {
  while (my_node != NULL) {
    printf("%d\n", my_node->data);
    printf("%s\n", my_node->name);
    my_node = my_node->next;
  }
}

int main() {
	struct person *head = NULL;

	sorted_insert(&head, 2, "Michael");
	sorted_insert(&head, 92, "Brian");
	sorted_insert(&head, 33, "Joe");
	sorted_insert(&head, -33, "Mike");
	sorted_insert(&head, 0, "John");
	
	print_list(head);

	return 0;
}
```

#### TODO: swap elements in a list of three nodes

doesnt seem possible for a human being.\
thinking of different ways, maybe sorted inserts.

The position of each node is defined by two pointers:\
Its next pointer and the next pointer of the previous node.\
If the node is the first node, there is no previous node, but the head points to it.\
In this case you have to change the head, that is passed to the function.\
But just swapping those two pointers from one node with the two pointers of another node will not work because it will overwrite the pointers of the other node...


TODO: general algorithm

this worked for swapping node 1 and 2 in a linked list of three nodes as well as swapping node 0 and 1:
```
void swap_1_and_2(struct person **my_head) {
	if (*my_head == NULL)
		return;
	
	struct person *tmp = NULL;

	tmp = (**my_head).next;
	(*my_head)->next = (*my_head)->next->next;
  (*my_head)->next->next = tmp;
	(*tmp).next = NULL;
}

void swap_0_and_1(struct person **my_head) {
	if (*my_head == NULL) {
		printf ("list is empty\n");
		return;
	}

	struct person *tmp = NULL;
	tmp = (**my_head).next;
	(**my_head).next = (*(**my_head).next).next;
	(*tmp).next = *my_head;
	*my_head = tmp;
}
```

#### TODO: bubble sorting a list

How can I sort a list when I can't access the nodes directly and can't pass each node by reference to the function?

Turns out you can just change the values of the members of all nodes like this:
```
void change_all(struct prices *my_head) {
  while (my_head != NULL) {
    my_head->price = 99;
    my_head = my_head->next;
  }
}
```

chatgpt was able to do it. It works by swapping the members (data fields):
```
void swap(struct Node* a, struct Node* b) {
    int temp = a->data;
    a->data = b->data;
    b->data = temp;
}

void bubble_sort(struct Node* head) {
    int swapped;
    struct Node* current;

    if (head == NULL)
        return;

    swapped = 1;
    while(swapped) {
        swapped = 0;
        current = head;

        while (current->next != NULL) {
            if (current->data > current->next->data) {
                swap(current, current->next);
                swapped = 1;
            }
            current = current->next;
        }
    }
}
```
I wonder if it is possible not by swapping "data", but by swapping the nodes.

https://www.geeksforgeeks.org/bubble-sort-for-linked-list-by-swapping-nodes/\
there is C code which seems to work

***

#### free memory of a linked list
After calling the following function like this `free_list(head);` there was no leaked memory left in valgrind:
```
void free_list(struct event *head)
{
   struct event* tmp;

   while (head != NULL)
    {
       tmp = head;
       head = head->next;
       free(tmp);
    }
}
```
***

A simple example of how to fill a linked list and print all members of the list:
```
#include <stdio.h>
#include <stdlib.h>

struct Node {
  int data;
  struct Node* next;
};

// This function prints contents of linked list starting
// from the given node
void printList(struct Node* n)
{
  while (n != NULL) {
    printf(" %d ", n->data);
    n = n->next;
  }
}

int main()
{
  struct Node* first = NULL;
  struct Node* second = NULL;
  struct Node* third = NULL;

  // allocate 3 nodes in the heap
  first = (struct Node*)malloc(sizeof(struct Node));
  second = (struct Node*)malloc(sizeof(struct Node));
  third = (struct Node*)malloc(sizeof(struct Node));

  first->data = 1; // assign data in first node
  first->next = second; // Link first node with second

  second->data = 2; // assign data to second node
  second->next = third;

  third->data = 3; // assign data to third node
  third->next = NULL;

  // Function call
  printList(first);

  return 0;
}
```

#### array of structs

The data of the nodes is filled manually and they are connected manually.\
Printing the data of all nodes is done with a while loop.\
I think this is **not a real linked list**. It is closer to an array than a linked list.\
It is more of a struct array. You can probably leave out the next member and the linked list parts in the code\
and just treat it like a normal array.
```
#include <stdio.h>
#include <string.h>

struct event {
  char summary[100];
  char date[11];
  struct event *next;
} events[100];

int main() {
  strcpy(events[0].summary, "eat dinner");
  strcpy(events[0].date, "2023-08-21");
  strcpy(events[1].summary, "eat lunch");
  strcpy(events[1].date, "2024-10-10");

  // connect the two nodes
  events[0].next = &events[1];

  // close the list
  events[1].next = NULL;
  
  // print data from single nodes
  printf ("%s\n", events[0].summary);
  printf ("%s\n", events[0].next->summary);

  struct event head = events[0];

  // print the entire linked list
  struct event *p = &head;
  while (p != NULL) {
    printf("%s\n", p->summary);
    printf("%s\n", p->date);
    p = p->next;
  }

  return 0;
}
```

#### save strings from an .ics file into an array of structs

But in this example it becomes clear that sometimes you might want to dynamically allocate memory (use malloc),\
because otherwise you would have to declare a very big array size for the `struct event events[]` array.\
You have to give at least 100000 max array elements for it and even then an extremely big .ics file will crash your application.

On the other hand when you allocate memory dynamically, an enormous .ics file will crash your computer.

```
#include <stdio.h>
#include <string.h>
#include <limits.h>

struct event {
  char summary[100];
  char date[11];
  struct event *next;
} events[100];

int main() {
  char my_line[LINE_MAX] = "";

  char my_file_name[100] = "calendar.ics";
  FILE *my_file = fopen(my_file_name, "r");

  int i = 0;
  while(fgets(my_line, LINE_MAX, my_file)) {
    if (strncmp(my_line, "SUMMARY", 7) == 0) {
      strcpy(events[i].summary, my_line);
      // connect the node to the next node
      events[i].next = &events[i+1];
      i++;
    }
  }
  // define the end of the list
  events[i].next = NULL;

  struct event head = events[0];

  // print the entire linked list
  struct event *p = &head;
  while (p != NULL) {
    printf("%s\n", p->summary);
    p = p->next;
  }

  return 0;
}
```

#### push new node to the front of the list

This example works. (not that the summary member is not really used here)\
But it is easy to adjust to also include the summary.\
The names of the nodes are only used as local variables inside functions.\
That means that the nodes can't be accessed by name and you don't need to give nodes names, which is good.
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct event {
  char summary[100];
  int date;
  struct event *next;
};

struct event* build_one_two_three() {
  struct event* head = NULL;
  struct event* second = NULL;
  struct event* third = NULL;
  head = malloc(sizeof(struct event));
  second = malloc(sizeof(struct event));
  third = malloc(sizeof(struct event));
  head->date = 1;
  head->next = second;
  second->date = 2;
  second->next = third;
  third->date = 3;
  third->next = NULL;

  return head;
}

void push(struct event** head_ref, int data) {
  struct event* newNode = malloc(sizeof(struct event));
  newNode->date = data;
  newNode->next = *head_ref;
  *head_ref = newNode;
}

int main() {
  struct event *head = build_one_two_three();

  push(&head, 125);
  push(&head, 13);

  // print the entire linked list
  struct event *p = head;
  while (p != NULL) {
    printf("%s\n", p->summary);
    printf("%d\n", p->date);
    p = p->next;
  }

  return 0;
}
```
