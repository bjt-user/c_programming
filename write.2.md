## append to a file

This worked
```
	int myfd = open(file_name, O_APPEND | O_WRONLY);

	char new_event_buf[4096] = "test\n";

	write(myfd, new_event_buf, sizeof(new_event_buf));

	close(myfd);
```
But the file looks now differently in vim.\
It has a lot of `^@` symbols at the end of the file and shows the line endings with `^M`.

To prevent from writing `^@` characters use strlen instead of sizeof:
```
write(myfd, new_event_buf, strlen(new_event_buf));
```

The `^M` characters were present in `vim` because I now had two kinds of line endings in the file: `\r\n` (windows) and `\n`
