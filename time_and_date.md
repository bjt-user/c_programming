#### get the current unix timestamp

```
#include <stdio.h>
#include <time.h>

int main() {
  time_t current_time;

  //current_time = time (&current_time);
  time (&current_time);

  printf ("%ld\n", current_time);

  return 0;
}
```
The function `time()` returns the current unix timestamp, but also takes a pointer to a time_t variable, that will take on the value of the current timestamp.\
You need the argument anyway, so assigning a variable to the return value is redundant.

`time_t` datatype seems to be a long, so you need `%ld` to print it with printf.

#### localtime

The function `localtime()` takes a pointer to a `time_t` unix timestamp and returns a pointer to a `struct tm`.\
(see "broken-down time" for `struct tm` explanation)

This example prints the current seconds.\
The `%02d` in the printf means "the width of the integer is 2 and it is smaller than 10 add a 0, if it is smaller than 1 output 2 0s."
```
#include <stdio.h>
#include <time.h>

int main() {
  struct tm *my_time_struct_ptr;

  time_t now_ts;

  time(&now_ts);

  my_time_struct_ptr = localtime(&now_ts);
  printf ("%02d\n", my_time_struct_ptr->tm_sec);

  return 0;
}
```

So you can print the current local time formatted for human readability like this:
```
#include <stdio.h>
#include <time.h>

int main() {
  struct tm *my_time_struct_ptr;

  time_t now_ts = time(&now_ts);

  my_time_struct_ptr = localtime(&now_ts);
  printf ("%02d:%02d:%02d\n",\
  my_time_struct_ptr->tm_hour,\
  my_time_struct_ptr->tm_min,\
  my_time_struct_ptr->tm_sec);

  return 0;
}
```
TODO: check if the strftime() function is more compact and more readable.

#### strftime

You can use the function `strftime` with a char array or a char pointer.\
But it seems like you have to use malloc on the char pointer, otherwise gcc will complain about the use of an\
uninitialized variable.
```
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
  time_t my_unix_ts = time(&my_unix_ts);
  struct tm *my_tm_local = localtime(&my_unix_ts);
  char* timestring = malloc(100);
  strftime (timestring,100,"%A %B %d", my_tm_local);

  printf ("%s\n", timestring);

  return 0;
}
```
or like this with a char array (and the time printed like "%H:%m:%s"):
```
#include <stdio.h>
#include <time.h>

int main() {
  time_t my_unix_ts = time(&my_unix_ts);
  struct tm *my_tm_local = localtime(&my_unix_ts);
  char timestring[100];
  strftime (timestring,100,"%T", my_tm_local);

  printf ("%s\n", timestring);

  return 0;
}
```

#### function to get the current date
This function needs a buffer that is big enough to hold the string.
```
#include <stdio.h>
#include <time.h>
#include <string.h>

void get_date(char buffer[]) {
  // add 1 because strlen does not include the null character
  size_t buffer_size = strlen(buffer) + 1;
  time_t my_unix_ts = time(NULL);
  struct tm* my_tm_local = localtime(&my_unix_ts);
  strftime(buffer, buffer_size, "%Y-%m-%d", my_tm_local);
}

int main() {
    char current_date[] = "xxxx-xx-xx"; // Buffer to store the date string
    get_date(current_date);

    printf("Current date: %s\n", current_date);

    if (current_date[strlen(current_date)] == '\0') {
      printf ("current_date is null terminated.\n");
    } else {
      printf ("current_date is NOT null terminated.\n");
    }

    return 0;
}
```

#### struct tm
```
man 3 tm
```

It is important that the member `tm_year` expects the year to be the number of years since 1900!\
And the month in the member `tm_mon` is expected from 0-11. (0 is january, 11 is december)

#### sleeping

This program prints the current timestamp, sleeps for 10 seconds, then prints the current timestamp again.
```
#include <stdio.h>
#include <time.h>
#include <unistd.h>

int main() {
  time_t current_time;
  int sleep_time = 10;

  time (&current_time);
  printf ("%ld\n", current_time);

  sleep (sleep_time);

  time (&current_time);
  printf ("%ld\n", current_time);

  return 0;
}
```

There is also a `nanosleep()` function.\
https://www.gnu.org/software/libc/manual/html_mono/libc.html#index-nanosleep
***

#### broken-down time

https://www.gnu.org/software/libc/manual/html_node/Broken_002ddown-Time.html

***

#### alarm function

The alarm function is defined in the `unistd.h` header.\
And takes the number of seconds until the entire program exits. ("receives a signal").\
The function is executed, then the rest of the program is executed, but in the background the timer is running and when it has finished, the program exits.

```
#include <stdio.h>
#include <unistd.h>

int main() {
  alarm(10);
  while (1) {
    sleep(1);
  }

  return 0;
}
```

```
$ ./a.out 
Alarm clock
$ echo $?
142
```

#### get timezone (in Europe/Berlin format)

to resolve symlinks:
```
man 3 realpath
```

```
#include <stdio.h>
#include <stdlib.h>

int main() {
	char result[256] = "";
	realpath("/etc/localtime", result);
	printf ("%s\n", result);
	return 0;
}
```

** Be careful: `readlink` is a POSIX function, not a C function **

You can get the timezone in the format of "CEST" through the struct tm,\
but this code resolves the symlink of /etc/localtime and shows it in the format of `/usr/share/zoneinfo/Europe/Berlin`.

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main() {
    char timezone_path[1024] = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

    ssize_t bytes_read = readlink("/etc/localtime", timezone_path, sizeof(timezone_path) - 1);

    if (bytes_read != -1) {
        timezone_path[bytes_read] = '\0'; // Null-terminate the string
        printf("The symlink /etc/localtime points to '%s'\n", timezone_path);
    } else {
        perror("readlink");
        return 1; // Error handling
    }

    return 0;
}
```

#### convert date string to unix timestamp

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main() {
	char *date_str = malloc(256);
	struct tm my_tm = {0};
	time_t unix_timestamp = 0;

	strcpy(date_str, "20231122");
	int my_date = atoi(date_str);

	printf ("%s\n", date_str);

	printf ("%d\n", my_date);

	my_tm.tm_year = my_date / 10000 - 1900;
	my_tm.tm_mon = (my_date % 10000) / 100 - 1;
	my_tm.tm_mday = (my_date % 100);

	printf("%d\n", my_tm.tm_year);
	printf("%d\n", my_tm.tm_mon);
	printf("%d\n", my_tm.tm_mday);

	unix_timestamp = mktime(&my_tm);

	printf ("%ld\n", unix_timestamp);

	return 0;
}
```

#### strptime - convert some formatted date into struct tm

This converts a formatted date "YYYYMMDD" into a unix timestamp.

```
#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main() {
    const char *date_str = "20230101";
	time_t unix_timestamp = 0;

	// initialize the tm struct to all zeros
	struct tm tm = {0};

    if (strptime(date_str, "%Y%m%d", &tm) != NULL) {
        unix_timestamp = mktime(&tm);
        if (unix_timestamp != -1) {
            printf("Unix Timestamp: %ld\n", unix_timestamp);
        } else {
            printf("Error: mktime() failed\n");
        }
    } else {
        printf("Error: strptime() failed\n");
    }

    return 0;
}
```

THe line
``` 
#define _XOPEN_SOURCE
```
fucked up one of my includes though...the readlink function (POSIX function) is now implicitly defined
