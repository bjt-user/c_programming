It should be possible to use structs without using `malloc`.

```
...
const int MAX_NODES = 100;

struct Node {
    int data;
    int next;
};

Node nodes[MAX_NODES];

...
```
