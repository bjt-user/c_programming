`#include <assert.h>`

`man 3 assert`

This will abort the program:
```
assert(0);
```

So you can put an expression in brackets to form an exit condition.

#### example: ensure that a variable is equal to a string

```
#include <stdio.h>
#include <string.h>
#include <assert.h>

int main() {
  char my_string[] = "I am a cool dude.\n";

  assert(!strcmp(my_string, "I am a cool dude.\n"));
  printf ("my_string: %s\n", my_string);

  memset(my_string, '\0', sizeof(my_string));

  assert(!strcmp(my_string, "I am a cool dude.\n"));
  printf ("my_string: %s\n", my_string);

  return 0;
}
```

#### example: ensure that a substring is part of a string

```
#include <stdio.h>
#include <string.h>
#include <assert.h>

int main() {
  char my_string[] = "I am a cool dude.\n";

  assert(strstr(my_string, "I am a cool dude.\n"));
  printf ("my_string: %s\n", my_string);

  memset(my_string, '\0', sizeof(my_string));

  assert(strstr(my_string, "I am a cool dude.\n"));
  printf ("my_string: %s\n", my_string);

  return 0;
}
```

```
$ ./a.out 
my_string: I am a cool dude.

a.out: main.c:13: main: Assertion `strstr(my_string, "I am a cool dude.\n")' failed.
Aborted (core dumped)
```
