#### Why do you need malloc?

source: chatgpt
```
char *mycharp;
mycharp = "how to allocate memory???";
```

The string literal "how to allocate memory???" is typically stored in a read-only memory segment, \
and mycharp is pointing to that read-only memory. You cannot modify the contents of the string literal using mycharp.

In contrast, when you allocate memory dynamically using malloc, \
you are requesting a block of memory from the heap that can be used to store data.\
The allocated memory is not read-only, and you can modify its contents.

#### coding without malloc

Safety critical applications are typically not developed with heap memory allocation (malloc):\
https://en.wikipedia.org/wiki/The_Power_of_10:_Rules_for_Developing_Safety-Critical_Code

#### malloc inside functions

This works:
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

void malloc_func(char **ptr) {
        *ptr = malloc(256);
}

int main() {
        char *myptr = NULL;
        malloc_func(&myptr);

        assert (myptr != NULL);
        strcpy(myptr, "OHHI");

        printf ("%p\n", myptr);
        printf ("%s\n", myptr);
        free(myptr);

        return 0;
}
```
You have to dereference the char pointer inside the function since call by reference is used here.

`strcpy` only works on allocated memory.\
So this will produce a segfault:
```
#include <stdio.h>
#include <string.h>

int main() {
        char *my_string = "HELLOTHERE";

        strcpy(my_string, "TEST");

        return 0;
}
```
