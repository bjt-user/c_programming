#### multiple conditions

This works:
```
if (myvar == 44 || secvar == 5) {
    printf("FOOO\n");
}
```

Separating the expressions with braces `(` does not work.
```
main.c:7:26: error: expected expression before ‘||’ token
    7 |         if (myvar == 44) || (secvar == 5) {
      |
```
