## using 0 and 1

C doesnt have bools out of the box.\
A lot of programs just use the integers 0 or 1 for false and true.

#### toggling

To "switch to the opposite value" you can use the negation operator `!`.

This will output `1`:
```
#include <stdio.h>

int main() {
	int my_bool = 0;

	my_bool = !my_bool;

	printf("%d\n", my_bool);
}
```

If you negate a non zero integer value, it will evaluate to 0.

## using stdbool.h

```
#include <stdio.h>
#include <stdbool.h>

int main() {
	printf("%d\n", false);
	printf("%d\n", true);
}
```
This will output:
```
0
1
```
