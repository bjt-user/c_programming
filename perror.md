`man 3 perror`

```
int myfd = open(ICS_PATH, O_RDONLY);
if (myfd == -1) {
	perror ("Error opening file");
	return 1;
}
```

> The perror() function produces a message on standard error describing the last error encountered during a call to a system or library function.

Seems really handy, because you don't have to get errno and lookup error codes.
