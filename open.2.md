```
man 2 open
```

#### overwrite a file

This clears the contents of an existing file by truncating its "length" to zero.
```
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main() {
	int myfile = open("myfile.txt", O_TRUNC);

	close(myfile);

	return 0;
}
```
(The length of a file is the number of bytes in it.)

***

#### flags

> The argument flags must include one of the following access modes: O_RDONLY, O_WRONLY, or O_RDWR.  These request opening the file read-only, writeonly, or read/write, respectively.

O_NONBLOCK\
O_RDONLY\
O_WRONLY\
O_RDWR

***

#### close the file

```
man 2 close
```
