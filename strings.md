#### strstr: find substrings

use `strstr` from `string.h`
```
char *strstr(const char *haystack, const char *needle);
```

Returns a pointer to the beginning of the located substring, or NULL if the substring is not found.

Example that just uses `strstr` to detect if the substring is present but doesnt use the return value:
```
if (strstr(my_line, "BEGIN:VEVENT")) {
  printf("%s", my_line);
}
```

You can use `strstr` to get the index of a substring inside a string:
```
#include <stdio.h>
#include <string.h>

int main() {
        char my_string[256] = "helloSUMMARYmorestuffokokoktheend";

        char *my_substr = strstr(my_string, "morestuff");

        int index_of_morestuff = my_substr - my_string;

        printf ("index_of_morestuff: %d\n", index_of_morestuff);

        return 0;
}
```

and you can use a combination of `strstr` and `strncpy` to get a substring that goes from a substring until a substring:
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
        char my_string[] = "BEGIN:VEVENT\r\nSUMMARY: a very long string string string\r\nLOCATION: paris, france\r\n";

        char *summary = strstr(my_string, "SUMMARY");
        char *location = strstr(my_string, "LOCATION");

        char *summary_only = malloc(256);
        memset(summary_only, '\0', 256);
        strncpy(summary_only, summary + strlen("SUMMARY: "), location - summary - strlen("\r\n") - strlen("SUMMARY: "));

        printf ("summary: %s\n", summary);
        printf ("location: %s\n", location);
        printf ("summary_only: %s\n", summary_only);

        free(summary_only);

        return 0;
}
```

or with a for loop instead of `strncpy`:
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
        char my_string[] = "BEGIN:VEVENT\r\nSUMMARY: a very long string string string\r\nLOCATION: paris, france\r\n";

        char *summary = strstr(my_string, "SUMMARY");
        char *location = strstr(my_string, "LOCATION");

        char *summary_only = malloc(256);
        memset(summary_only, '\0', 256);

        for (int i = 0; i < (location-summary-strlen("\r\n")); i++) {
                summary_only[i] = summary[i];
        }

        printf ("summary: %s\n", summary);
        printf ("location: %s\n", location);
        printf ("summary_only: %s\n", summary_only);

        free(summary_only);

        return 0;
}
```

#### starts with

```
#include <stdio.h>
#include <string.h>

int main() {
    char mystring[100] = "foobar";

    if (strncmp(mystring, "foo", 3) == 0) {
        printf("Bingo! It starts with foo!\n");
    } else {
        printf("No, it does NOT start with foo...\n");
    }

    return 0;
}
```

#### string is equal to

The `strcmp` returns 0 if the two arguments are the same string.
```
#include <stdio.h>
#include <string.h>

int main() {
  char my_string[] = "hello world.\n";

  if (!strcmp(my_string, "hello world.\n")) {
    printf ("The strings are equal.\n");
  } else {
    printf ("The strings are not equal.\n");
  }

  return 0;
}
```

#### truncating strings

Usually it should be enough to just set a '\0' to the desired place in the char array:
```
my_string[5] = '\0';
```

If you want to replace all remaining characters with null characters, you can use memset like this:\
(that might lead to less confusion, for example when using gdb or when unexpected things happen)
```
#include <stdio.h>
#include <string.h>

int main() {
    char myString[100] = "This is a long string.";

    myString[8] = '\0';

    // Fill the remaining elements after the null terminator with '\0'
    memset(&myString[9], '\0', sizeof(myString) - 9);

    printf("Truncated string: %s\n", myString); // Output: "This is "

    return 0;
}
```

#### strtok: splitting strings

`strtok` gives you the first portion of a string until a certain delimiter (can be multiple chars).\
When you call `strtok` a second time with NULL as argument, it gives you the second portion and so on.\
So you can use a while loop to get all parts.

**Be careful: `strtok` modifies the original string by putting null characters at every delimiter.**\
So the original string is not usable anymore after this operation.

```
#include <stdio.h>
#include <string.h>

int main() {
	char my_string[] = "welcome to the world.\n";
	char delim[] = " ";

	char *result1 = "";
	char *result2 = "";

	result1 = strtok(my_string, delim);
	result2 = strtok(NULL, delim);

	printf ("%s\n", result1);
	printf ("%s\n", result2);

	return 0;
}
```

`strtok` returns NULL if there is no part left to return.

#### comparing dates and time

This seems to work for comparing dates:
```
	char date1[] = "20200101";
	char date2[] = "20230101010100";

	if (strcmp(date1, date2) > 0) {
		printf ("date1 is bigger\n");
	} else {
		printf ("date2 is bigger\n");
	}
```
It works even when one date only contains the date and the other date also contains a time like this "20230101010100".\
Also when date and time is in a format like this `20240101T015959`.

#### strchr - locate character in string

```
man 3 strchr
```

>The strchr() function returns a pointer to the first occurrence of the character c in the string s.\
The strrchr() function returns a pointer to the last occurrence of the character c in the string s.

> Return value: The strchr() and strrchr() functions return a pointer to the matched character or NULL if the character is not found.\
The terminating null byte is considered part of the string, so that if c is specified as '\0', these functions return a pointer to the terminator.

#### strcat: concatenate strings

```
#include <stdio.h>
#include <string.h>

int main() {
	char my_buffer[128] = "test";
	strcat(my_buffer, "hello");

	printf ("%s\n", my_buffer); // testhello

	return 0;
}
```

#### check if string is null terminated

There is no univeral and consistent way to make sure your char pointer is null terminated.\
You might use things like strlcpy.

chatgpt proposed this:
```
#include <stdio.h>

int isNullTerminated(const char *str) {
    // Iterate through the characters until null character is found
    for (int i = 0; str[i] != '\0'; ++i) {
        // Do nothing, just loop until null character is found
    }

    // If the loop ends, the string is null-terminated
    return 1;
}

int main() {
    const char myString[] = "Hello, World!";

    if (isNullTerminated(myString)) {
        printf("The string is null-terminated.\n");
    } else {
        printf("The string is not null-terminated.\n");
    }

    return 0;
}
```

`gdb` might help:
```
(gdb) p hw
$9 = 0x555555556004 "hello world"
(gdb) p hw[10]
$10 = 100 'd'
(gdb) p hw[11]
$11 = 0 '\000'
```
