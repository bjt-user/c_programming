```
valgrind ./a.out
```

The number in front of the outputs `==1673==` represents the process id.

#### still reachable

There a different opinion if this needs to be fixed.\
It seems that the pulseaudio simple API does leave some still reachable memory even after using the function to free memory.\
It is probably not a big deal.
