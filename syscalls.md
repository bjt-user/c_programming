https://en.wikibooks.org/wiki/The_Linux_Kernel/Syscalls

It looks like to use the write syscall in C you need to include the unistd.h header.

```
#include <stdio.h>
#include <unistd.h>

int main() {
  write(1, "hello", 5);

  return 0;
}
```
The first argument of write means: 1 - output to stdout.\
The last argument of write is the length of the output, if you use 4 this outputs "hell", if you use more than 5, it outputs "hello".

#### TODO: use syscall "write" to write to a file

```
man 2 write
```

```
#include <unistd.h>

ssize_t write(int fd, const void *buf, size_t count);
```

https://www.gnu.org/software/libc/manual/html_node/Streams-and-File-Descriptors.html
> File descriptors are represented as objects of type int, while streams are represented as FILE * objects.

```
man 2 open
```

The `open` syscall returns a file descriptor.

Tried it like this:
```
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main() {
  int myfd = open("/home/bf/coding/c_programming/writetofile/test.txt", 'w');
  write(myfd, "written from c", 10);

  return 0;
}
```
But doesnt write anything to file and the file has very weird permissions:
```
---sr-x---. 1 bf bf     0 Mar 28 13:58 test.txt
```

***

#### open a file and write to it

This code created a new file if it does not exist, opens it for read and write, and then writes a string to it:
```
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main() {
  int myfd = open("/home/bf/workspace/c_programming/openandwrite/test.txt", O_CREAT|O_RDWR, S_IRWXU);

  if (myfd == -1) {
    puts ("open failed");
  } else {
    printf ("file descriptor: %d\n", myfd);
  }

  int write_result = write(myfd, "written from c", 14);

  printf ("write result: %d\n", write_result);

  return 0;
}
```

You can or the flags together like shown here: `O_CREAT|O_RDWR`.\
`S_IRWXU`  00700 user (file owner) has read, write, and execute permission.

O_CREAT means create file if it doesnt exist.\
O_RDWR means open for read and write.

The third argument for `open()` is the mode. (file permissions)

***

#### open file in nonblocking mode

```
#include <stdio.h>
#include <fcntl.h>

int main() {
  int result_open = open("/dev/snd/pcmC1D0c", O_RDWR|O_NONBLOCK);

  if (result_open == -1) {
    perror("The following error occurred during the syscall \"open\"");
  }

  return 0;
}
```
This will output:
```
The following error occurred during the syscall "open": Device or resource busy
```

If you dont use the `O_NONBLOCK` flag, the syscall will be blocked forever.

***
