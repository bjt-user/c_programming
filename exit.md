```
man 3 exit
```

`exit()` from `stdlib.h` can be used to exit the program from inside a function\
as `return` will only exit the function but not the entire program

`exit(1)` for return code 1
