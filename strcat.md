`man 3 strcat`

```
char *strcat(char *dest, const char *src);
```

**return value**: a pointer to the first argument, which contains the concatenated string

## concatenate char arrays

The `strcat` function expects two **null-terminated** char arrays.\
Remember to give your char array one additional character/byte for the null terminator.
```
#include <stdio.h>
#include <string.h>

int main() {
  char one[5] = "ello\0";
  char two[10] = "h\0";

  strcat(two, one);

  printf(two);

  return 0;
}
```
```
$ ./a.out
hello
```

Note: When you initialize the char array two with just 2 bytes, the program still works, but it works because of luck and undefined behavior.\
You should always make sure that your char array contains enough bytes to hold what is concatenated by strcat.

Because the sizeof(two) will not change due to strcat.

```
#include <stdio.h>
#include <string.h>

int main() {
  char one[5] = "ello\0";
  char two[2] = "h\0";

  strcat(two, one);

  printf("%s\n", two);
  printf("%ld\n", sizeof(two));

  return 0;
}
```
```
$ ./a.out
hello
2
```
