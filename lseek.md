```
man 2 lseek
```

headers:
```
#include <unistd.h>
```

You can **set** the **file position**.

Argument 1: file descriptor\
Argument 2: the number of bytes you want to move forward/backwards\
Argument 3: change the position from the current position (`SEEK_CUR`) or from the beginning (`SEEK_SET`) or end of the file (`SEEK_END`)

#### move the position of the file backwards one byte
```
lseek(myfd, -1, SEEK_CUR);
```

#### seeking before beginning of the file

When you seek previous to the beginning of the file position, lseek returns -1 and errno is set to 22 which means EINVAL.
> EINVAL whence is not valid.  Or: the resulting file offset would be negative, or beyond the end of a seekable device.
```
int ret_lseek = lseek(myfd, -3, SEEK_CUR);
if (ret_lseek == -1) {
  printf ("return code of lseek: %d\n", ret_lseek);
  printf ("errno: %d\n", errno);
  printf ("EINVAL: %d\n", EINVAL);
}
```
