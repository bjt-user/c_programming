inet_ntop makes a IPv4 string from a binary IP.

The first argument is AF_INET which means IPv4.\
AF_INET6 should mean IPv6.\
It takes a void pointer as the second argument that points to the binary IP.

This program outputs the IP of google.com:
```
#include <stdio.h>
#include <arpa/inet.h>

int main() {
  int ipbin = 0b10101110001001001111101110001110;
  void *ipbinptr = &ipbin;
  char ipstr[INET_ADDRSTRLEN];

  inet_ntop(AF_INET, ipbinptr, ipstr, sizeof(ipstr));

  printf ("%s\n", ipstr);

  return 0;
}
```
The binary of the IP came from debugging a getaddrinfo program with gdb.

This program outputs: `142.251.36.174`

#### how the calculation works

The bytes are received in network byte order.\
So the first byte of the binary is the last number of the IP.

0b 10101110 00100100 11111011 10001110

142 - 10001110 (the last byte)\
251 - 11111011\
36 - 00100100\
174 - 10101110 (the first byte)

In hexadecimal `ipbin` would be `0xae24fb8e`.\
8e - 142\
fb - 251\
24 - 36\
ae - 174

***
