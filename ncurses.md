#### TODO: minimal example

trying to get a minimal example running\
copying all include files to the source folder\
and compile a static binary that runs on systems where ncurses is not installed

***

this should work if you have all the right includes...\
just the header file is not enough you need to find where the functions are declared int he lib
```
#include "ncurses.h"

int main(int argc, char **argv) {
  initscr();

  refresh();

  getch();

  endwin();

  return 0;
}
```

#### initscr function

This `sed` command starts printing the contents of the file when it sees `initscr(void)` at the beginning of a line\
and ends printing when it finds a `}` at the beginning of a line.
```
cat ~/workspace/ncurses/ncurses/base/lib_initscr.c | sed -n '/^initscr(void)/,/^}/p'
```
(sed might help in finding more functions in source code)

But there are a lot of nested includes in that source file, and some are not in the repo...like `ncurses_cfg.h`.

***
