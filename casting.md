#### cast char array to integer

Use the `atoi` function from `stdlib.h`:
```
#include <stdio.h>
#include <stdlib.h>

int main() {
    char arr[13] = "2023o, World!";

    int arr_int = atoi(arr);

    printf("Result: %s\n", arr);
    printf("Result: %d\n", arr_int);

    int my_result = arr_int + 5;
    printf ("%d\n", my_result);

    return 0;
}
```
