#### mailing list

you need to create an account first

https://lists.freedesktop.org/mailman/listinfo/pulseaudio-discuss

or learn how to register at OTFC irc network

***

```
sudo apt install libpulse-dev
```

you will need to link when compiling like this: (the libraries **need** to be at the **end** of the command...)
```
gcc -Wall main.c -lpulse -lpulse-simple -lm
```

this produces about 1 second of sound (22050 buffer size) in 440hz:
```
#include <stdio.h>
#include <pulse/error.h>
#include <pulse/simple.h>
#include <math.h>
#include <errno.h>

int main(int argc, char*argv[]) {
    int i;
    short buffer[22050];
    pa_simple *s;
    pa_sample_spec ss = {
        .format = PA_SAMPLE_S16LE,
        .rate = 44100,
        .channels = 1
    };
    float frequency = 440.0;

    for (i = 0; i < 22050; i++) {
        buffer[i] = (short)(32767.0 * sin(2.0 * M_PI * frequency * i / 44100.0));
    }

    if (!(s = pa_simple_new(NULL, argv[0], PA_STREAM_PLAYBACK, NULL, "playback", &ss, NULL, NULL, &errno))) {
        fprintf(stderr, "pa_simple_new() failed: %s\n", pa_strerror(errno));
        return 1;
    }

    if (pa_simple_write(s, buffer, sizeof(buffer), NULL) < 0) {
        fprintf(stderr, "pa_simple_write() failed: %s\n", pa_strerror(errno));
        return 1;
    }

    pa_simple_drain(s, NULL);

    pa_simple_free(s);

    return 0;
}
```

#### alarm

This is a self made alarm.\
The comments tell you how to configure it.\
For example you could set the `buffer_size` to 16384 to make longer sounds.
```
#include <stdio.h>
#include <pulse/error.h>
#include <pulse/simple.h>
#include <math.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>

int main(int argc, char*argv[]) {
    // buffer_size will configure the length of the  tones
    int buffer_size = 8192; 
    short buffer[buffer_size];
    pa_simple *s;
    pa_sample_spec ss = {
        .format = PA_SAMPLE_S16LE,
        .rate = 44100,
        .channels = 1
    };

    int number_of_tones = 10;
    for (int i = 0; i < number_of_tones; i++) {
      if (i % 2 != 0) {
        // the length of the pause between tones
        sleep(1);
        continue;
      }

      // configure the frequency
      float frequency = 700.0;

      for (int j = 0; j < buffer_size; j++) {
          buffer[j] = (short)(32767.0 * sin(2.0 * M_PI * frequency * j / 44100.0));
      }

      if (!(s = pa_simple_new(NULL, argv[0], PA_STREAM_PLAYBACK, NULL, "playback", &ss, NULL, NULL, &errno))) {
          fprintf(stderr, "pa_simple_new() failed: %s\n", pa_strerror(errno));
          return 1;
      }

      if (pa_simple_write(s, buffer, sizeof(buffer), NULL) < 0) {
          fprintf(stderr, "pa_simple_write() failed: %s\n", pa_strerror(errno));
          return 1;
      }
    }

    pa_simple_drain(s, NULL);

    pa_simple_free(s);

    return 0;
}
```
compile like this:
```
gcc -Wall main.c -lpulse -lpulse-simple -lm
```

***

#### play wave file

This will only work with raw `.wav` files, not with compressed files like `.ogg`.

To convert fro .ogg to .wav:
```
ffmpeg -i /usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga alarm.wav
```

```
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <pulse/simple.h>
#include <pulse/error.h>

int main() {
    char content[800000];

    int myfd = open("alarm.wav", O_RDONLY);

    read(myfd, content, sizeof(content));

    close(myfd);

    pa_simple* simple = NULL;
    pa_sample_spec ss;
    ss.format = PA_SAMPLE_S16LE;
    ss.rate = 48000;
    ss.channels = 2;

    simple = pa_simple_new(NULL, "Audio Playback", PA_STREAM_PLAYBACK, NULL, "playback", &ss, NULL, NULL, NULL);

    pa_simple_write(simple, content, sizeof(content), NULL);

    pa_simple_drain(simple, NULL);

    pa_simple_free(simple);

    return 0;
}
```

What is strange: If you use `ss.channels = 1` and `ss.rate` of 44100, the sound file plays at half its speed.\
When you then use 96100 as `ss.rate` it seems to play almost at normal speed again.

I think you should adjust this to whatever `file audiofile.wav` says:
```
$ file alarm.wav 
alarm.wav: RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, stereo 48000 Hz
```
"stereo" -> `ss.channels = 2`\
"48000 Hz" -> `ss.rate = 48000`

**TODO**: you have to play the file chunk by chunk (maybe 1024 bytes) so that it stops writing to pulseaudio at EOF.\
Also you probably need to start after the file header. Maybe at byte 44.\
I think right now it is outputting the file header as sound.
Then you can build in a check on the file header, that gives an error if the file is not a RIFF/WAVE file.

Improved program that reads only 1024 bytes of data at a time and therefore stops writing to pulseaudio when file is fully read:
```
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <pulse/simple.h>
#include <pulse/error.h>

int main() {
    char content[1024];

    int myfd = open("alarm.wav", O_RDONLY);

    pa_simple* simple = NULL;
    pa_sample_spec ss;
    ss.format = PA_SAMPLE_S16LE;
    ss.rate = 48000;
    ss.channels = 2;

    simple = pa_simple_new(NULL, "Audio Playback", PA_STREAM_PLAYBACK, NULL, "playback", &ss, NULL, NULL, NULL);

    while (read(myfd, content, sizeof(content))) {
      pa_simple_write(simple, content, sizeof(content), NULL);
    }

    pa_simple_drain(simple, NULL);

    pa_simple_free(simple);

    close(myfd);

    return 0;
}
```

***

## troubleshooting

```
Failed to load cookie file from cookie: Not a directory
```

A workaround was to redirect stderr to `/dev/null` for `pa_simple_new` like this:
```
    // Temporarily redirect stderr to /dev/null
	// because pulseaudio prints to stderr even though it does not set any error codes
    FILE *original_stderr = stderr;
    stderr = fopen("/dev/null", "w");

    simple = pa_simple_new(NULL, "Audio Playback", PA_STREAM_PLAYBACK, NULL, "playback", &ss, NULL, NULL, &pa_error);

	// Restore stderr
    stderr = original_stderr;
```

But later I found the cause of this bug: I actually changed the `HOME` environment variable inside my C program.\
Because `getenv` returns a pointer to the actual environment variable and not just a copy.
