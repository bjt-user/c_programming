strlen gives the number of characters in a char array and not the allocated size
```
#include <stdio.h>
#include <string.h>

int main() {
  char my_var[100] = "hello";

  printf ("%ld\n", strlen(my_var));

  return 0;
}
```

```
$ ./a.out
5
```

strlen does not include the terminating null character.\
so `char my_var[100] = "hello\0";` is still 5.

#### get the last character of a char array

```
printf ("%c\n", my_var[strlen(my_var)-1]);
```

That way you can for example add a character at the end of a char array:
```
#include <stdio.h>
#include <string.h>

int main() {
  char my_var[100] = "hello\0";

  // add a character to the string
  char my_char = 'z';
  my_var[strlen(my_var)] = my_char;
  printf ("%s\n", my_var);

  return 0;
}
```
```
$ ./a.out
helloz
```

#### strlen of an empty char array is 0

strlen of an empty char array is 0\
and sizeof of an empty char array is the amount of allocated bytes/characters
```
#include <stdio.h>
#include <string.h>

int main() {
  char my_var[100] = "";

  printf ("%ld\n", strlen(my_var));
  printf ("%ld\n", sizeof(my_var));

  return 0;
}
```
```
$ ./a.out
0
100
```
