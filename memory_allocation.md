#### arrays with a fixed size

In C, when you declare an array with a fixed size, the memory for that array is allocated on the stack.\
Once the array goes out of scope (in this case, when the program execution leaves the current function), the memory is automatically released.

So with arrays with a fixed size you dont need to allocate and free memory.
