```
printf("Line: %d\n",__LINE__);
printf("File: %s\n",__FILE__);
printf("Func: %s\n",__func__);
```

## segmentation faults

Looks like you can use `gdb` and just type `r` without setting breakpoints:
```Program received signal SIGSEGV, Segmentation fault.
0x0000555555555610 in swap (my_head=0x7fffffffdfe0, a=0, b=2) at main.c:121
121		tmp1 = (*a_prev).next;
```
And it shows you the line where the segfault occurs.\
But it doesn't always show you a line in your code.\
When it does not show a line in your code you can type `bt` (for backtrace) in gdb after the segfault\
and then you should see a line number you can work with.

Segfaults seem to happen often, when you try to access the member of a null pointer:
```
(*a_prev).next
```
if a_prev points to null you cannot dereference it, and there will probably be a segfault.

#### using valgrind

`valgrind` might help to debug seg faults.

```
$ valgrind icscli
...
==5110== Invalid read of size 1
==5110==    at 0x484EFFF: strncpy (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
==5110==    by 0x10B522: parse_event (in /usr/local/bin/myprogram)
==5110==    by 0x10B2FA: main (in /usr/local/bin/myprogram)
```

It tells me that the problem is in function `parse_event`.\
And inside a `strncpy` call.\
Maybe I can use the hex codes at the beginning of the line somehow.
