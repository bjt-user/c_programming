`head->summary` is equivalent to `(*head).summary`

These two lines print similar output:
```
printf("%s\n", (*head).summary);
printf("%s\n", head->summary);
```

With linked list you usually work with pointers to structs, so you need to dereference those pointers to access the members.

Code might be easier to read if you do not use `->` operators at all?!
