The header file `netdb.h` contains the struct addrinfo.
```
$ cat /usr/include/netdb.h | grep -ni -A8 "struct addrinfo"
565:struct addrinfo
566-{
567-  int ai_flags;			/* Input flags.  */
568-  int ai_family;		/* Protocol family for socket.  */
569-  int ai_socktype;		/* Socket type.  */
570-  int ai_protocol;		/* Protocol for socket.  */
571-  socklen_t ai_addrlen;		/* Length of socket address.  */
572-  struct sockaddr *ai_addr;	/* Socket address for socket.  */
573-  char *ai_canonname;		/* Canonical name for service location.  */
574:  struct addrinfo *ai_next;	/* Pointer to next in list.  */
575-};
```

#### defining the struct addrinfo

If you define a struct addrinfo called myaddrinfo like this:
```
#include <stdio.h>
#include <netdb.h>

int main() {

  struct addrinfo myaddrinfo;

  return 0;
}
```
you can actually see the variable myaddrinfo in the debugger.
```
(gdb) print myaddrinfo 
$1 = {ai_flags = 0, ai_family = 0, ai_socktype = 0, ai_protocol = 0, ai_addrlen = 0, ai_addr = 0x7ffff7fe6380, ai_canonname = 0x0, 
  ai_next = 0x7ffff7ffdab0 <_rtld_global+2736>}
```

***

#### parameter 1: node

Either node or service, but not both, may be NULL.

node specifies either a numerical network address (for IPv4, numbers-and-dots notation as supported by inet_aton(3); for IPv6, hexadecimal string format as supported by inet_pton(3)), or a **network hostname**, whose network addresses are looked up and  resolved.\
If hints.ai_flags contains the AI_NUMERICHOST flag, then node must be a numerical network address.  The AI_NUMERICHOST flag suppresses any potentially lengthy network host address lookups.

#### parameter 2: service

service sets the port in each returned address structure.
If this argument is a service name (see services(5)), it is translated to the corresponding port number.\
This argument can also be specified as a decimal number, which is simply converted to binary.

```
cat /etc/services | grep "https"
https             443/tcp
https             443/udp
https             443/sctp
...
```

However when I specify a decimal number I get this gcc output:
```
note: expected ‘const char * restrict’ but argument is of type ‘int’
  661 |                         const char *__restrict __service,
```

#### parameter 3: const struct addrinfo *restrict hints

The hints argument points to an addrinfo structure.



***

#### executing the function with rc 0 and no warnings

This code will return 0 from the functions and show no gcc warnings at all:
```
#include <stdio.h>
#include <netdb.h>

int main() {
  struct addrinfo myaddrinfo;
  struct addrinfo* myaddrinfoptr;
  myaddrinfoptr = &myaddrinfo;

  int rc;
  rc = getaddrinfo("google.com", "https", myaddrinfoptr, &myaddrinfoptr);

  printf ("%d\n", rc);

  return 0;
}
``` 

But I could not retrieve an IP with gdb.\
You probably need to set some fields in the addrinfo struct.

***

#### small example that gets IP from hostname

I got a working example from chatgpt and simplified it:

```
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>

int main(int argc, char *argv[]) {
    char* hostname = "google.com";

    struct addrinfo hints, *result, *p;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;    // allow only IPv4
    hints.ai_socktype = SOCK_STREAM; // TCP stream sockets

    int status = getaddrinfo(hostname, NULL, &hints, &result);
    if (status != 0) {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        return 2;
    }

    char ipstr[INET_ADDRSTRLEN];
    for (p = result; p != NULL; p = p->ai_next) {
        void *addr;
        char *ipver;

        struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
        addr = &(ipv4->sin_addr);
        ipver = "IPv4";

        // convert the IP to a string and print it:
        inet_ntop(p->ai_family, addr, ipstr, sizeof(ipstr));
        printf("%s: %s\n", ipver, ipstr);
    }

    freeaddrinfo(result); // free the linked list
    return 0;
}
```

TODO: run gdb on it and try to understand it better and simplify it more

inet_ntop - convert IPv4 and IPv6 addresses from binary to text form

This program also works for hosts in the local network not only internet hosts.

***
