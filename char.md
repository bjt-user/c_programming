I would always initialize an empty character like this:
```
char cur_char = '\0';
```
So that you do not have a random character in your char if you solely declare it.


#### check for whitespace (isspace)

```
#include <stdio.h>
#include <ctype.h>

int main() {
	char my_char = 'c';
	char a_space = ' ';
	char a_tab = '\t';
	char a_nl = '\n';
	char a_lf = '\r';

	printf ("%d\n", isspace(my_char));
	printf ("%d\n", isspace(a_space));
	printf ("%d\n", isspace(a_tab));
	printf ("%d\n", isspace(a_nl));
	printf ("%d\n", isspace(a_lf));

	return 0;
}
```

Output:
```
0
8192
8192
8192
8192
```

#### check for spaces and tabs (isblank)

```
#include <stdio.h>
#include <ctype.h>

int main() {
	char my_char = 'c';
	char a_space = ' ';
	char a_tab = '\t';
	char a_nl = '\n';
	char a_lf = '\r';

	printf ("isblank:\n");
	printf ("%d\n", isblank(my_char));
	printf ("%d\n", isblank(a_space));
	printf ("%d\n", isblank(a_tab));
	printf ("%d\n", isblank(a_nl));
	printf ("%d\n", isblank(a_lf));

	return 0;
}
```

Output:
```
isblank:
0
1
1
0
0
```
