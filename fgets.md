read an entire line

from `man fgets`:
> fgets()  reads in at most one less than size characters from stream and stores them into the buffer pointed to by s.\
Reading stops after an EOF or a newline.\
If a newline is read, it is stored into the buffer.\
A terminating null byte ('\0') is stored after the last character in the buffer.


POSIX defines the maximum size of input line...in limits.h
```
#include <stdio.h>
#include <limits.h>

int main() {
  printf("%d\n", LINE_MAX);

  return 0;
}
```
which was
```
2048
```
in my system

#### read the first line of a file

```
#include <stdio.h>
#include <limits.h>

int main() {
  char my_line[LINE_MAX] = "";
  FILE *my_file = fopen("myfile.txt", "r");

  fgets(my_line, LINE_MAX, my_file);

  printf("%s\n", my_line);

  return 0;
}
```

#### read all lines of a file

fgets returns NULL on EOF or error, so you can use a while loop:
```
#include <stdio.h>
#include <limits.h>

int main() {
  char my_line[LINE_MAX] = "";
  char my_file_name[100] = "myfile.txt";
  FILE *my_file = fopen(my_file_name, "r");

  while(fgets(my_line, LINE_MAX, my_file)) {
    my_line[4] = 'O';
    printf("%s\n", my_line);
  }

  return 0;
```

It might make sense to use the `stat` syscall before using fgets to check if the file exists, \
because fgets can't do that, the program will just crash and do nothing if the file doesnt exist.
