#### general

This file should explore how to control the terminal, e.g. clear the entire screen, clear a line, clear a character, etc.

You could use libraries like `ncurses`.\
(that needs to be installed first)\
`ncurses` seems to use `terminfo`.

There is a lot of text in
```
man 5 terminfo
```
Maybe if you have a lot of time you can get useful information out of that manpage.

ANSI escape codes:

https://notes.burke.libbey.me/ansi-escape-codes/

#### clear the entire terminal

```
write(1, "\033c", 3);
```
or do the same thing with a c function:
```
fputs("\033c", stdout);
```

or
```
write(1, "\ec", 3);
```
***

#### delete entire line

```
printf ("\r\033[2K");
```

#### delete from cursor until end of line

```
write(1, "\033[K", 3);
```

I tested this in this example program:
```
#include <stdio.h>
#include <unistd.h>

int main() {
  for (int i = 100000; i > 0; i/=100) {
    printf ("%d", i);
    fflush(stdout);
    sleep(1);
    // delete everything from cursor to end of line
    write(1, "\033[K", 3);
    sleep(1);
    // move cursor to beginning of the line
    printf("\r");
  }
  return 0;
}
```
Doesnt seem to work in every situation though...

***
#### move cursor up one line

```
printf("\033[1A");
```
To move up two lines use `2A` etc...

#### move cursor to specific column of the terminal

move cursor to column 10 of the terminal (**WARNING: ALWAYS CHANGE THE LAST ARGUMENT OF THE WRITE SYSCALL ACCORDINGLY**)
```
write(1, "\033[10G", 5);
```

move cursor to column 0:
```
write(1, "\033[0G", 4);
```

I wrote a nice small example program that takes advantage of this sequence:
```
#include <stdio.h>
#include <unistd.h>

int main() {
  write(1, "\033[10G", 5);
  printf ("hello world");
  fflush(stdout);
  sleep(1);
  write(1, "\033[10G", 5);
  printf ("hi    world");
  fflush(stdout);
  sleep(1);
  write(1, "\033[0G", 4);
  return 0;
}
``` 

***
