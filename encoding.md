https://en.wikipedia.org/wiki/Character_encoding

https://www.ascii-code.com/

I was reading a text file with a C program and this line from the text file:
```
DTSTAMP:20230719T152822Z
```
turned into something like this:
```
DTSTAMP:2023��V
```

=> the problem was not encoding...\
the array I was reading it into was only 11 elements long...

#### check encoding of a file

```
$ file -i calendar.ics 
calendar.ics: text/calendar; charset=utf-8
$ file -i main.c 
main.c: text/x-c; charset=us-ascii
```

#### reading a utf-8 file

When you read a file like this it will save the characters correctly automatically.
```
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main() {
  int myfd = open("calendar.ics", O_RDONLY);
  char buffer[4096];

  read(myfd, buffer, sizeof(buffer));

  printf("%s\n", buffer);

  close(myfd);
}
```

The problem seems to arrive once you read character by character.\
=> Nope. When I read char by char it is still displayed correctly...

```
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main() {
  int myfd = open("calendar.ics", O_RDONLY);
  char buffer[4096] = "";

  for (int i = 0; i < sizeof(buffer); i++) {
    read(myfd, &buffer[i], 1);
  }

  printf("%s\n", buffer);


  close(myfd);
}
```
