parsing command line options (for long options there is `getopt_long`)

`man 3 getopt`

```
#include <unistd.h>

int getopt(int argc, char *argv[], const char *optstring);

extern char *optarg;
extern int optind, opterr, optopt;
```

#### example 1: short options

This worked for giving the program a file name with `-f`, a certain number of minutes with `-m`, and the usage with `-h`.
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv) {
  int opt = 0;
  int minutes = 0;

  char *file_name = "";

  while ((opt = getopt(argc, argv, "f:hm:")) != -1) {
    switch(opt)
    {
    case 'f':
    file_name = optarg;
    break;
    case 'h':
    printf ("this is the help!\n");
    return 0;
    case 'm':
    minutes = atoi(optarg);
    }
  }


  printf ("%s\n", file_name);
  printf ("%d\n", minutes);

}
```
