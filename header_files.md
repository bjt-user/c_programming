http://websites.umich.edu/~eecs381/handouts/CHeaderFileGuidelines.pdf

How to work with multiple c source code files and how to use header files with function definitions correctly?

In the header file you have the function definitions and the includes, and then you dont need those in the .c file anymore.\
You never include .c files.\
But in some projects I see includes in .c and .h files.\
=> I need to do more research on that topic. 

You can view the preprocessed output like this (but its a lot of code):
```
gcc -E main.c go_back_x_lines.c | less -N
```

#### duplicate headers

The compiler does not complain if you have multiple includes with the same header file:
```
#include <stdio.h>
#include <stdio.h>
#include <stdio.h>
```
Also the output of `gcc -E main.c` will only increase by one line (or 5 lines when it is your own header file) when you add a duplicate header file.

By using include guards or `#pragma once` (a non-standard but widely supported preprocessor directive) in your .h file, you ensure that the header file is included only once, even if multiple source files include it.\
This prevents duplicate function definitions and related issues during the linking phase.

**Include guards**, also known as header guards or macro guards, are a simple technique used in C and C++ to prevent a header file from being included more than once in the same source file.

It looks like this:
```
#ifndef MY_HEADER_H
#define MY_HEADER_H

// Content of your header file (function declarations, structure definitions, etc.)

#endif // MY_HEADER_H
```
So if you accidently include your header twice, the compiler will just skip it the second time.

#### include guards vs. pragma once

https://en.wikipedia.org/wiki/Include_guard

https://en.wikipedia.org/wiki/Pragma_once

> In the C and C++ programming languages, #pragma once is a non-standard but widely supported preprocessor directive designed to cause the current source file to be included only once in a single compilation.

`#pragma once` is less code, but has some caveats, that is why it is not standardized yet.\
I think I will use `#pragman once` because the caveats don't seem to matter for normal projects.

#### working with multiple files

According to chatgpt this is how you would work with multiple files:
```
$ bat * --style="header-filename"
File: a.out   <BINARY>

File: hello_world.c
#include "hello_world.h"
#include <stdio.h>

void hello_world() {
  printf ("hello world\n");
}

File: hello_world.h
void hello_world();

File: main.c
#include "hello_world.h"

int main() {
  hello_world();

  return 0;
}
```

compile:
```
gcc -Wall main.c hello_world.c
```
```
./a.out
```

#### check if any function from header file is used

You can use a script like this:
```
#!/bin/bash

# give the header file without file extension as arg1
header_file=$1

functions=($(cat "${header_file}.h" | grep ";$" | cut -d" " -f2 | cut -d"(" -f1))

source_files=($(find . -name "*.c" -type f | grep -v "${header_file}.*"))

for source_file in ${source_files[@]}; do
	echo "${source_file}:"
	for function in ${functions[@]}; do
		cat "$source_file" | grep "$function"
	done
done

exit 0
```

Usage: put it as a bash alias and go into your source directory and run:
```
check_function_usage my_header_file
```

=> If no function of that file is used in any source file: you can delete header and source file.
