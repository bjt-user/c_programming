Welcome to hell.

>The unary operator & gives the address of an object. (K&R page 93)

What are "unary operators"?\
operators that operate on only one argument.

>The unary operator * is the indirection or dereferencing operator; when applied to a pointer, it accesses the object, the pointer points to. (K&R page 94)

## experiments

#### dereferencing a non-pointer

When you try to dereference a variable, that is not a pointer, gcc will say something like this:
```
main.c:11:3: error: invalid type argument of unary ‘*’ (have ‘int’)
   11 |   *a = 3;
```
The `'int'` in `(have 'int')` is the data type of `a` (the variable you are trying to dereference).

#### function calls by reference

```
#include <stdio.h>

void change_me(int *a) {
	printf ("&a (inside change_me): %p\n", &a);
	printf ("a (inside change_me): %p\n", a);
	printf ("*a (inside change_me): %d\n", *a);
	*a = 3;
}

int main() {
	int a = 1;

	change_me(&a);

	printf ("%d\n", a);

	return 0;
}
```
```
&a (inside change_me): 0x7ffe63e65008
a (inside change_me): 0x7ffe63e65024
*a (inside change_me): 1
3
```
When using "by reference" functions you need to say `*my_var` to access the value of the parameter.\
To access the address of the parameter you simply say `my_var`.\
So inside the function `a` is what `&a` is outside the function and `*a` is what `a` is outside the function.\
`&a` inside the function seems to be the address of the pointer to a. (maybe a temporary pointer that is created by the call by reference)

**Realization:**\
So what seems to happen behind the scenes is:\
When you do a call by reference:
```
void foo(int *bar) {
...
```
```
foo(&my_var);
```
A local pointer to int called `bar` is created within function `foo`.\
And it points to `my_var`.\
It is similar to doing this in the main function:
```
int *bar = &myvar;
```
Only that bar is a local variable that is only accessible in the function `foo`.
