#### warning: ‘struct foo’ declared inside parameter list will not be visible outside of this definition or declaration

The order of `#include`s is very imortant!

If you declare a struct in a `.h` file you have to include that first\
before including other files that use this struct.
