#### read entire file into string

This seems to work but be careful!\
fread does NOT set the null terminator!\
You have to set the null terminator yourself.\

This MIGHT set the null terminator correctly:
```
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	if (argc < 2) {
		printf ("Please provide a file as argument one.\n");
		return 1;
	}

	char* file_name = argv[1];

	FILE* my_file = fopen(file_name, "r");

	long length = 0;

	char* buffer = "";

	fseek(my_file, 0, SEEK_END);

	length = ftell(my_file);

	fseek(my_file, 0, SEEK_SET);

	buffer = malloc(length+1);

	fread(buffer, length, 1, my_file);

	buffer[length] = '\0';

	printf ("%s\n", buffer);

	return 0;
}
```

It doesn't seem possible to check if a string is null terminated \
because you don't know how long the string is and the length of the string is \
defined by the null terminator.
