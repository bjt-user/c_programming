## useful flags

compile with debug info:
``` 
gcc -g main.c
```

compile with all warnings: (might give hints why your program does not do what you want)
```
gcc -Wall main.c
```

make assembler code from c source code file:
```
gcc -S hw.c
```
=> But you can't just assemble and link that assembly code, because you would need to link the resulting\
object file to the C library. Otherwise it will not be able to find the function "printf" for example.

***

#### ISO standard

https://gcc.gnu.org/onlinedocs/gcc/Standards.html

> The default, if no C language dialect options are given, is -std=gnu17. 

#### run c program as script

to test small things:
```
$ cat c_script.c
//usr/bin/gcc -Wall "$0" && exec ./a.out "$@"
#include <stdio.h>

int main(){
        printf ("hello world\n");
        return 0;
}
```
You need a `.c` extension.\
```
chmod +x c_script.c
./c_script.c
```
