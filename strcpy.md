## trap

I tried to clear a string with null characters like this:
```
strcpy(my_string, "");
```
but that **only** replaces **the first character** of char array `my_string` with a `'\0'`.\
The rest of the string remains the same.

printf will show an empty string, but gdb shows you what really happened:
```
(gdb) p my_string
$5 = "\000. testo\n", '\000' <repeats 246 times>
```
And then when you copy characters at the beginning of the string you will have unexpected outputs...
