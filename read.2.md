`man 2 read`

```
#include <unistd.h>

ssize_t read(int fd, void *buf, size_t count);
```

#### reading/writing with system calls vs. standard library functions

Reading one byte at a time is relatively slow with the `read` system call.\
I heard that `fread` is faster because it buffers.\
So using system calls might be better when you know the structure of your file very well \
and using standard library functions and file pointers is better when you have to process \
unknown files with different formats.\
Searching for strings character by character is probably a lot faster with `fread`.

#### example to read a file into a char array

```
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main() {
    char content[100];

    int myfd = open("test.txt", O_RDONLY);

    read(myfd, content, sizeof(content));

    close(myfd);

    printf("Content of the file:\n%s\n", content);

    return 0;
}
```

#### read file character by character and save every line but overwrite it each time

```
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int main() {
  char my_line[1024] = "";
  char my_char;
  int cal_file_fd = open("my_calendar.txt", O_RDONLY);

  if (cal_file_fd == -1) {
    perror("Failed to open file");
    return 1;
  }

  int i = 0;
  while (read(cal_file_fd, &my_char, 1) > 0) {
    my_line[i] = my_char;
    i++;
    if (my_char == '\n') {
      printf ("%s\n", my_line);
      i = 0;
      # clear my_line
      memset(my_line, '\0', sizeof(my_line));
    }
  }

  close(cal_file_fd);

  return 0;
}
```

#### read file into char array by determining the file size with stat

```
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

long int get_file_size(char file_name[])
{
        struct stat my_stat;
        stat(file_name, &my_stat);
        return my_stat.st_size;
}

int main()
{
        int myfd = open("test.txt", O_RDWR);

        long int my_filesize = get_file_size("test.txt");

        printf("file size: %ld\n", my_filesize);

        char *mybuffer = malloc(my_filesize + 1);

        int rc_read = read(myfd, mybuffer, my_filesize);

        // read does not automatically add a null character at the end
        mybuffer[my_filesize] = '\0';

        printf("rc_read: %d\n", rc_read);
        printf("mybuffer: %s\n", mybuffer);

        close(myfd);

        free(mybuffer);

        return 0;
}
```

#### reading at EOF

If you are at the end of a file, read **returns 0**, and **nothing will be written to the variable** at argument 2.\
This can lead to confusion because the variable you are reading into will remain unchanged.

***
