#### resources

Resources: There is a free ebook on dbooks.org with 881 pages from Richard Stallman, Roland Pesch, Stan Shebs, et al.

https://www.cs.umd.edu/~srhuang/teaching/cmsc212/gdb-tutorial-handout.pdf

#### start gdb

You start gdb on the executable you generated with gcc.
```
gdb a.out
```

But you should compile your program with debug symbols.

```
gcc -g mysource.c
```

debug a program that uses cli arguments:
```
gdb --args executablename arg1 arg2 arg3
```

#### quit gdb without prompt

```
$ tail -1 ~/.gdbinit 
set confirm off
```

#### setting breakpoints

Setting a breakpoint in the first line:
```
break 1
```

or to set a breakpoint at the first line of the main function:
```
break main:1
```

view all breakpoints:
```
info breakpoints
```

delete a breakpoint:
```
delete 1
```

#### stepping through code

`step` to move to the next line\
`step` seems to be a "step into" (step into functions)

you can type `next` or short `n`\
that seems to be a "step over" (step over functions)

there is also `nexti` which moves to the next assembly instruction (short `ni`)

just hitting <kbd>enter</kbd> without any input **repeats the last command**

#### print variables

```
(gdb) print result
$6 = 10
(gdb) print result
$7 = 10
```

The $7 seems pretty useless it just accumulates everytime you use the `print` command.

or shorter:
```
(gdb) p result
``` 

print all local variables
```
info locals
```

to print all variables (extremely long list) (this doesnt seem very useful, but it is there):
```
info variables
```

to print the registers:
```
info reg
```

to print the value of the variable in binary/hexadecimal/decimal:
```
(gdb) print myaddrinfo.ai_addr
$3 = (struct sockaddr *) 0x7ffff7fe6380
(gdb) p/t myaddrinfo.ai_addr
$4 = 11111111111111111110111111111100110001110000000
(gdb) p/x myaddrinfo.ai_addr
$5 = 0x7ffff7fe6380
(gdb) p/d myaddrinfo.ai_addr
$6 = 140737354032000
```

https://ftp.gnu.org/old-gnu/Manuals/gdb/html_node/gdb_54.html

#### show current line

```
frame
```

#### list

`list` - "lists the ten lines before a previous ten-line listing."

```
list 5,6
5	  int second = 6;
6	  int result = first + second;
```

```
(gdb) list 1
1	#include <stdio.h>
2	
3	int main() {
4	  int first = 4;
5	  int second = 6;
6	  int result = first + second;
7	  printf ("result: %d\n", result);
8	  return 0;
9	}
```

```
(gdb) list main
1	#include <stdio.h>
2	
3	int main() {
4	  int first = 4;
5	  int second = 6;
6	  int result = first + second;
7	  printf ("result: %d\n", result);
8	  return 0;
9	}
```

You can configure to show more than 10 lines:
```
(gdb) set listsize 20
```

#### layout

```
layout next
``` 
shows a window of assembly code first, when you type it a second time it shows your c code

```
layout src
```
for your c code

``` 
layout asm
```
for the assembly code

short:
```
lay asm
```

or to view the changes of the registers:
```
lay reg
```
(registers might not be readable at the first line of the main method, but become visible after the first `ni`)

You can leave the "tui" mode with <kbd>ctrl</kbd> + <kbd>x</kbd> + <kbd>a</kbd>

#### help

The help is overwhelming so you have to specifiy the command or the realm of the command you need.
```
help list
```

#### debuginfod

https://wiki.archlinux.org/title/Debuginfod

The purpose seems to be: "Now a debugger can fetch debug symbols without having to install the appropriate debug package."

For a start I disable it because it is annoying if you only want to debug your own programs:
```
$ cat ~/.gdbinit
set debuginfod enabled off
```

#### debugging linked lists

The last three slides of this presentation might help:\
https://www.cs.umd.edu/~srhuang/teaching/cmsc212/gdb-tutorial-handout.pdf

```
(gdb) print second.data
$1 = 2
(gdb) print second->data
$2 = 2
(gdb) print second.next.data
$3 = 3
```
In `gdb` the `.` and the `->` seem to do the same.\
You can do tabcompletion while printing elements of linked lists: (like hitting <kbd>tab</kbd> twice for every possibility)
```
(gdb) print second.next.
data next
```

iterating over linked lists:

https://interrupt.memfault.com/blog/advanced-gdb#the--variable

Instead of adding .next_thread onto the end of the list each iteration, you can use the value of $ and just keep pressing <enter>.
```
(gdb) p mgmt_thread_data.next_thread
$67 = (struct k_thread *) 0x2000188c <eswifi0+48>
(gdb) p $.next_thread
$68 = (struct k_thread *) 0x200022ac <eswifi_spi0+20>
(gdb) <enter>
$69 = (struct k_thread *) 0x2000a120 <k_sys_work_q+20>
```

#### registers

show all registers:
```
i reg
```

print value of register rax:
```
p $rax
```

print hex value of register rax:
```
(gdb) p/x $rax
$6 = 0xee
```

as binary:
```
(gdb) p/t $rax
$8 = 11101110
```

watch register rax:
```
(gdb) watch $rax
Watchpoint 2: $rax
(gdb) ni
0x000000000040112f	4		printf("hello world\n");
(gdb) ni

Watchpoint 2: $rax

Old value = 4198694
New value = 238
_dl_runtime_resolve_xsave () at ../sysdeps/x86_64/dl-trampoline.h:108
108		xorl %edx, %edx
(gdb)
```

Remember to step through code with `ni` (next instruction)

The registers only contain the addresses where values are stored in memory.

To print the value of an address a register is pointing to:
```
(gdb) x/s $rdi
0x402010:	"hi"
```
(So "hi" is at address 0x402010 in memory, but it is not stored directly in rdi register)\
But at 0x402010 there is only the first character "h". The "i" should be at the next memory address.

```
(gdb) x/c 0x402010
0x402010:	104 'h'
(gdb) x/c 0x402011
0x402011:	105 'i'
```

```
(gdb) help x
Examine memory: x/FMT ADDRESS.
ADDRESS is an expression for the memory address to examine.
FMT is a repeat count followed by a format letter and a size letter.
Format letters are o(octal), x(hex), d(decimal), u(unsigned decimal),
  t(binary), f(float), a(address), i(instruction), c(char), s(string)
  and z(hex, zero padded on the left).
Size letters are b(byte), h(halfword), w(word), g(giant, 8 bytes).
The specified number of objects of the specified size are printed
according to the format.  If a negative number is specified, memory is
examined backward from the address.
```

#### watchpoints

Watching a string: (doesnt work before running the program, because the variable will then not be available in the current scope)
```
break 1
run
watch my_string
c(ontinue)
```
(this may take a while...)

You can stop the program if a string matches a specific string:
```
watch $_streq(my_string, "hello")
```

***

You can watch single characters:
```
watch my_string[0]
```

But you can do a `display` to strings, which is not as powerful as a watchpoint,\
because it prints the string everytime the debugger stops. (after every command):
```
(gdb) display my_string 
1: my_string = "\000\000\000\000\000\000\000\000 b\376\367\377\177\000\000\000\000"
(gdb) n
5	  char my_string[] = "I am a cool dude.\n";
1: my_string = "\000\000\000\000\000\000\000\000 b\376\367\377\177\000\000\000\000"
(gdb) 
6	  memset(my_string, '\0', sizeof(my_string));
1: my_string = "I am a cool dude.\n"
(gdb) 
8	  return 0;
1: my_string = '\000' <repeats 18 times>
```

#### conditional breakpoints

break when variable is equal to a string:
```
break 12 if $_streq(my_string, "hello")
```
`$_streq` is a gdb convenience function.\
Using `!strcmp` in the if clause does not work because the debugger doesn't know its return type\
even when it is casted to an int.

There seem to be other useful convenience functions like:\
```
$_memeq(buf1, buf2, length)
$_regex(str, regex)
$_streq(str1, str2)
$_strlen(str)
```

See here:\
https://sourceware.org/gdb/onlinedocs/gdb/Convenience-Funs.html

```
break 20 if $_strlen(result_buffer)>3
```

#### call functions inside gdb

```
call (size_t)strlen(part2)
```

***

## troubleshooting

`Missing separate debuginfos, use: dnf debuginfo-install glibc-2.36-9.fc37.x86_64`

The command `debuginfo-install` can be installed with the `dnf-utils` package.

The message does not appear anymore when you do this:
```
sudo dnf debuginfo-install glibc
```
***
