#### why you do not need to allocate memory for literal strings

A literal string is something like this:
```
char* myvar = "foo";
```
=> this is valid C and you do not need to allocate memory with `malloc()`.

You can never change this string inside your program!\
And because this is the case the string is stored inside the binary!\
You should be able to get these strings with the `strings` program.\
```
strings a.out
```

But when you for example need to concatenate strings the string \
has to be generated at runtime, so the string cannot be stored raw in the \
binary.\
That is why you need to allocate memory when concatenating strings.
