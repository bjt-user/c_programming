#### initializing char arrays

If you initialize a char array with a string, you can do so like this:
```
char one[] = "hello world";
```
and it will be null terminated.

```
#include <stdio.h>
#include <string.h>

int main() {
    char one[] = "hell";
	char two[4] = "hell";

    printf ("strlen(one): %ld\n", strlen(one));
	printf ("strlen(two): %ld\n", strlen(two));

	return 0;
}
```
will output:
```
strlen(one): 4
strlen(two): 8
```
because the char array `two` is not null terminated. (the strlen Output is probably undefined behavior)\
If you initialize it like this it is null terminated:
```
    char two[5] = "hell";
```

#### assigning char arrays

Unfortunatelly you can't just assign char arrays like in other programming languages.\
This example will **NOT** work:

```
#include <stdio.h>

int main() {
  char some_words[100] = "why strcpy???";

  char my_string[100] = some_words;

  printf ("%s\n", my_string);

  return 0;
}
```

You need `strcpy` function from the `string.h` library:
```
#include <stdio.h>
#include <string.h>

int main() {
  char some_words[100] = "why strcpy???";

  char my_string[100];
  strcpy(my_string, some_words);

  printf ("%s\n", some_words);
  printf ("%s\n", my_string);

  return 0;
}
```

With char pointers on the other hand you can just assign one char pointer to another:
```
#include <stdio.h>

int main() {
  char *some_words = "why strcpy???";

  char *my_string = some_words;

  printf ("%s\n", some_words);
  printf ("%s\n", my_string);

  return 0;
}
```

#### add a character to a char array

You can put a different character into an array or at the end of an array anywhere you want:
```
  char my_var[100] = "hello\0";

  my_var[5] = 'w';
  printf ("%s\n", my_var);
```

#### returning char arrays

chatgpt:
> A char array cannot be directly returned from a function as arrays decay into pointers.\
Instead, you can return a pointer to the first element of the array.
