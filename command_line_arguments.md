#### pass integer cli arg to main

To pass an integer to a C program:
```
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("No integer argument provided.\n");
        return 1;
    }
    
    int myInt = atoi(argv[1]);
    printf("The provided integer is: %d\n", myInt);

    // Rest of your code...

    return 0;
}
```

`man 3 atoi` says "atoi, atol, atoll - convert a string to an integer"

Only problem is: If you provide a string as a cli arg, you get `0`.

`man 3 atoi` says:
```
BUGS
       errno is not set on error so there is no way to distinguish between 0 as an error and as the converted value.
       No checks for overflow or  underflow  are done.  Only base-10 input can be converted.
       It is recommended to instead use the strtol() and strtoul() family of
       functions in new programs.
```

#### check if cli argument is an integer

```
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("No argument provided.\n");
        return 1;
    }

    char* argument = argv[1];

    int isInteger = 1;
    for (int i = 0; argument[i] != '\0'; i++) {
        if (!isdigit((unsigned char)argument[i])) {
            isInteger = 0;
            break;
        }
    }

    if (isInteger) {
        printf("The argument is an integer.\n");
        int intValue = atoi(argument);
        printf("The provided integer is: %d\n", intValue);
    } else {
        printf("The argument is not an integer.\n");
    }

    return 0;
}
```

#### TODO: use strtol for integer cli args

There is a good example program in `man 3 strtol`.
