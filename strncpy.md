get the first 4 characters of a char array:

```
#include <stdio.h>
#include <string.h>

int main() {
    char arr[13] = "Hello, World!";
    char result[5]; // Variable to store the first 4 characters and the null terminator

    strncpy(result, arr, 4); // Copy the first 4 characters from arr to result
    result[4] = '\0'; // Add the null terminator to indicate the end of the string

    printf("Result: %s\n", result);

    return 0;
}
```
