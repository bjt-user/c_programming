`#include <string.h>`

Good for "clearing" strings. (filling them with null characters)

```
(gdb) 
6	  memset(my_string, '\0', sizeof(my_string));
(gdb) list
1	#include <stdio.h>
2	#include <string.h>
3	
4	int main() {
5	  char my_string[] = "I am a cool dude.\n";
6	  memset(my_string, '\0', sizeof(my_string));
7	
8	  return 0;
9	}
(gdb) p my_string 
$1 = "I am a cool dude.\n"
(gdb) n
8	  return 0;
(gdb) p my_string 
$2 = '\000' <repeats 18 times>
```

#### inside functions

In functions you get warnings like this:
```
main.c:9:26: warning: ‘sizeof’ on array function parameter ‘buffer’ will return size of ‘char *’ [-Wsizeof-array-argument]
    9 |   printf ("%ld\n", sizeof(buffer));
```

You can also pass the size of the char pointer to the function and use that.\
Or use `strlen` to just zero out everything until the null terminator, but that does not guarantee that the string is completely filled with `'\0'`s.
