## fgets

```
man 3 fgets
```

> Reading stops after an EOF or a newline.\
If a newline is read, it is stored into the buffer.\
A terminating null byte ('\0') is stored after the last character in the buffer.

```
#include <stdio.h>


int main() {
	char input_buffer[64] = "";

	fgets(input_buffer, sizeof(input_buffer), stdin);

	printf("%s\n", input_buffer);

	return 0;
}
```
If you input more than 64 bytes, the program will not crash, but the string will only contain the first 64 characters.

You should be able to check if the string was truncated if the last character of the string is not a `\n`.

**Problem of fgets and friends: When you overflow your buffer the rest remains in stdin and destroy your next fgets**\
this will only get the first input if you overflow the first buffer:
```
#include <stdio.h>

int main() {
	char input_one[8] = "";
	char input_two[8] = "";

	printf ("First input: ");
	fgets(input_one, sizeof(input_one), stdin);
	printf ("Second input: ");
	fgets(input_two, sizeof(input_two), stdin);

	printf ("%s\n", input_one);
	printf ("%s\n", input_two);

	return 0;
}
```
So you always have to check the string the user transmitted.\
Or provide a very big buffer that is unlikely to be overflown by the user.

This worked well for checking if the input buffer overflowed:
```
if (! strchr(input_buffer, '\n')) {
	printf ("Input buffer overflow!\n");
	exit(1);
}
```
Because if you don't overflow your buffer it should contain a `\n` character.

#### make user input into char array
By using a char pointer that points to the first null terminator of a char array\
you can make the user input go directly into the char array:
```
#include <stdio.h>
#include <string.h>

int main() {
	char input_buffer[64] = "start: ";

	char *input_pointer = &input_buffer[strlen(input_buffer)];

	fgets(input_pointer, (sizeof(input_buffer)-strlen(input_buffer)), stdin);

	printf("%s\n", input_buffer);

	return 0;
}
```

And also check if the input has been truncated:
```
#include <stdio.h>
#include <string.h>

int main() {
	char input_buffer[64] = "start: ";

	char *input_pointer = &input_buffer[strlen(input_buffer)];

	fgets(input_pointer, (sizeof(input_buffer)-strlen(input_buffer)), stdin);

	printf("%s\n", input_buffer);

	if (strchr(input_pointer, '\n') == NULL)
		printf ("input has been truncated\n");

	return 0;
}
```

## scanf - input strings

This will only take your string until the first <kbd>space</kbd> you type:
```
#include <stdio.h>

int main() {
	char input_buffer[256] = "";

	scanf("%s", input_buffer);

	printf("%s\n", input_buffer);

	return 0;
}
```

chatgpt says you can do something like this:
```
scanf("%255[^\n]", input_buffer);
```
but that seems like an ugly solution.

## fgetc

get a single character from stdin

WARNING: it seems that fgetc does not consume the `\n` character you have to type to confirm your character.\
When you then use fgets afterwards it will not take any input from stdin.

But that seems also the case with `fgets`.\
How to use these functions repeatedly?
