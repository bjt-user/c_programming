A utility to handle code basis.\
It should work with a lot of programming languages.
```
ctags --list-languages | less
```


```
sudo pacman -S ctags
```

Go into your source directory.

```
ctags *
```
This will generate a "tags" file.

Then you can go into any source file with `vim`.\
Go with the cursor on a function call and hit <kbd>ctrl</kbd> + <kbd>]</kbd>.\
It will take you to the definition of the function.\
<kbd>ctrl</kbd>+<kbd>o</kbd> to go back.

jump to the tag "parse_event" from the command line:
```
vim -t parse_event
```

You can run `:!ctags -R` on the root directory of projects in many popular languages to generate a tags file filled with definitions and locations for identifiers throughout your project.
Once a tags file for your project is available, you can search for uses of an appropriate tag throughout the project like so:
```
:tag someClass
```

The commands `:tn` and `:tp` will allow you to iterate through successive uses of the tag elsewhere in the project.

#### local variables

By default local variables are not included in the tags file for C:
```
ctags --list-kinds=C
```

to include them do:
```
ctags --kinds-C=+l *
```

Then I can at least know which data type the variable has.

To know which value the variable was last assigned ctags might not help.\
But you could backward search the word under cursor with <kbd>#</kbd>.

#### search higher level dirs for tags file

https://stackoverflow.com/questions/5017500/vim-difficulty-setting-up-ctags-source-in-subdirectories-dont-see-tags-file-i

```
set tags=tags;/
```

#### browse system libraries

https://www.youtube.com/watch?v=TWog5NklSws \
minute 7:30

But in Arch Linux `/usr/src` only has a `linux` dir.

No C files are in there:
```
find /usr/src -iname '*.c' -type f
```

```
wget https://ftpmirror.gnu.org/glibc/glibc-2.38.tar.gz
```
```
tar -xvf glibc-2.38.tar.gz
```

This worked somehow:
```
ctags --recurse --languages=C * /tmp/glibc-2.38/*
```

But it lead me to the printf func in this file: `/tmp/glibc-2.38/sysdeps/ieee754/ldbl-opt/nldbl-printf.c`\
and I wanted to go into this file: `/tmp/glibc-2.38/stdio-common/printf.c`.
