```
man 3 basename
```

```
$ cat main.c 
#include <stdio.h>
#include <libgen.h>

int main() {
	char *my_path = "/usr/local/bin/hello";
	char *b_path = basename(my_path);

	printf ("%s\n", b_path);

	return 0;
}
$ ./a.out 
hello
```
