In C everything is evaluated from right to left, so this
```
h = &(*h)->next;
```
is the same as:
```
h = &((*h)->next);
```

TODO: read up in K&R about the order of things
