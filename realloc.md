This is a working function that reallocates the memory of a char pointer:
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void realloc_func(char **ptr) {
        *ptr = realloc(*ptr, 512);
}

int main() {
        char *my_var = malloc(8);

        realloc_func(&my_var);

        memset(my_var, 'n', 256);

        printf("sizeof(my_var): %ld\n", sizeof(my_var));
        printf("my_var: %s\n", my_var);

        return 0;
}
```

Memory address of the pointer seems to change after realloc.\
`%p` shows a different address when using `printf` with the variable.

#### realloc a char pointer to a literal string

What happens if a pointer points to a literal string and then you realloc it?

=> segfault

