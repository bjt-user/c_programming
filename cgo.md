#### simple example

file `hi.c`:
```
#include <stdio.h>

int get_one() {
	return 1;
}
```

file `main.go`:
```
package main

import "fmt"

// #include "hi.c"
import "C"

func main() {
	fmt.Println("start")
	num := C.get_one()

	fmt.Println(num)
}
```

outputs:
```
$ go run main.go
start
1
```
