#### resources

TODO: https://www.linuxjournal.com/article/6735

#### TODO:very high level approach

Use `mpg123` and include the audio file into your compiled binary.

Answer from chatgpt:\
Yes, you can embed the mp3 file into your C program and compile it as part of your binary. Here's how you can do it:

1. Convert the mp3 file to a C array using a tool like xxd or objcopy. For example, if your mp3 file is called myfile.mp3, you can use the following command to convert it to a C array:

```
xxd -i myfile.mp3 > myfile.h
```

This will create a new header file called myfile.h that contains a C array representing the contents of the mp3 file.

1. Include the header file in your C program using the #include directive. For example:

```
#include "myfile.h"
```

1. Use the C array to play the mp3 file. Here's an example:

```

#include <stdio.h>
#include <stdlib.h>

#include "myfile.h"

int main(void) {
    // Play the mp3 file using mpg123
    FILE *mp3file = fopen("myfile.mp3", "wb");
    fwrite(myfile_mp3, 1, myfile_mp3_len, mp3file);
    fclose(mp3file);
    system("mpg123 myfile.mp3");

    // Remove the temporary file
    remove("myfile.mp3");

    return 0;
}
```
This program writes the contents of the myfile_mp3 array to a temporary file called myfile.mp3, plays it using mpg123, and then removes the temporary file. Note that you'll need to link your program with the mpg123 library to use it in your C program.

#### alsa

ALSA (https://wiki.archlinux.org/title/Advanced_Linux_Sound_Architecture) is part of the kernel.

On Fedora 37 the alsa headers are not there...
`#include <alsa/asoundlib.h>`\
You probably need to install a package for these to arrive...

The package `alsa-lib-devel` looks interesting:
```
dnf repoquery -l alsa-lib-devel | less
```

It has header files and `doxygen` stuff (which is a tool to document source code).

To view the documentation:
``` 
/usr/share/doc/alsa-lib-devel/doxygen/html $ firefox index.html
```

In `pacman` this package is not available, but it looks like the docs are also online in this link:\
https://www.alsa-project.org/alsa-doc/alsa-lib/

You can compile this minimal example\
https://www.alsa-project.org/alsa-doc/alsa-lib/_2test_2pcm_min_8c-example.html \
with
```
gcc -Wall -lasound main.c
```
but I dont hear sound.\
Maybe I have to select a different playback device:
```
static char *device = "default";            /* playback device */
```


> The usual place for default definitions is at `/usr/share/alsa/alsa.conf`.

Sound cards have an ID (a string), an index (an int, sometimes called the "card number"), a name, a longname, a mixername and a "components" property. The file /proc/asound/cards lists most of these properties for physical sound cards. Virtual sound cards are not listed in that file. The format is:

index [ID     ] Driver - name
                longname

***

#### alsa minimal example

First make sure your default device is configured correctly.\
Like this:
```
$ cat /proc/asound/cards
0 [MS             ]: USB-Audio - Jabra UC VOICE 550a MS
                     Jabra Jabra UC VOICE 550a MS at usb-0000:00:14.0-2.1, full speed
1 [PCH            ]: HDA-Intel - HDA Intel PCH
                     HDA Intel PCH at 0xd0810000 irq 96

$ cat /etc/asound.conf 
defaults.pcm.card 0
defaults.ctl.card 0
```

compile this with `gcc -Wall -lasound main.c`:
``` 
#include <alsa/asoundlib.h>
 
static char *device = "default";            /* playback device */
unsigned char buffer[16*1024];              /* some random data */
 
int main(void)
{
    int err;
    unsigned int i;
    snd_pcm_t *handle;
    snd_pcm_sframes_t frames;
 
    for (i = 0; i < sizeof(buffer); i++)
        buffer[i] = random() & 0xff;
 
    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }
    if ((err = snd_pcm_set_params(handle,
                      SND_PCM_FORMAT_U8,
                      SND_PCM_ACCESS_RW_INTERLEAVED,
                      1,
                      48000,
                      1,
                      500000)) < 0) {   /* 0.5sec */
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }
 
        for (i = 0; i < 16; i++) {
        frames = snd_pcm_writei(handle, buffer, sizeof(buffer));
        if (frames < 0)
            frames = snd_pcm_recover(handle, frames, 0);
        if (frames < 0) {
            printf("snd_pcm_writei failed: %s\n", snd_strerror(frames));
            break;
        }
        if (frames > 0 && frames < (long)sizeof(buffer))
            printf("Short write (expected %li, wrote %li)\n", (long)sizeof(buffer), frames);
    }
 
    /* pass the remaining samples, otherwise they're dropped in close */
    err = snd_pcm_drain(handle);
    if (err < 0)
        printf("snd_pcm_drain failed: %s\n", snd_strerror(err));
    snd_pcm_close(handle);
    return 0;
}
```

But it only works if it is the only sound source.\
If for example you have sound coming from your browser and pulse audio connects to alsa it will not work.\
You do **not** have to disable pulseaudio.\
But pulseaudio doesnt seem to block alsa when there is no sound coming from pulseaudio.

TODO: understand the bitwise and with 0xff:\
https://stackoverflow.com/questions/67677638/why-do-a-bitwise-and-of-a-character-with-0xff

#### alsa different pitches

Not sure why this works, but if you put a different multiplier to your buffer when filling it in a loop,\
you can adjust the pitch.
```
#include <alsa/asoundlib.h>
 
static char *device = "default";            /* playback device */
unsigned char buffer[16*1024];
 
int main(void)
{
    int err;
    unsigned int i;
    snd_pcm_t *handle;
    snd_pcm_sframes_t frames;
 
    for (i = 0; i < sizeof(buffer); i++) {
        //printf ("sizeof(buffer) is %ld\n", sizeof(buffer));
        //buffer[i] = i;
        //buffer[i] = i*2;
        buffer[i] = i*4;
        //buffer[i] = i*8;
    }
 
    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }
    if ((err = snd_pcm_set_params(handle,
                      SND_PCM_FORMAT_U8,
                      SND_PCM_ACCESS_RW_INTERLEAVED,
                      1,
                      48000,
                      1,
                      500000)) < 0) {   /* 0.5sec */
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }
 
        for (i = 0; i < 16; i++) {
        frames = snd_pcm_writei(handle, buffer, sizeof(buffer));
        if (frames < 0)
            frames = snd_pcm_recover(handle, frames, 0);
        if (frames < 0) {
            printf("snd_pcm_writei failed: %s\n", snd_strerror(frames));
            break;
        }
        if (frames > 0 && frames < (long)sizeof(buffer))
            printf("Short write (expected %li, wrote %li)\n", (long)sizeof(buffer), frames);
    }
 
    /* pass the remaining samples, otherwise they're dropped in close */
    err = snd_pcm_drain(handle);
    if (err < 0)
        printf("snd_pcm_drain failed: %s\n", snd_strerror(err));
    snd_pcm_close(handle);
    return 0;
}
```

#### pulsed tone

It looks like if you leave the second half of the chunk of data empty, you get a pulsed tone:
```
#include <alsa/asoundlib.h>
 
static char *device = "default";            /* playback device */
char buffer[8*1024];
 
int main(void)
{
    int err;
    unsigned int i;
    snd_pcm_t *handle;
    snd_pcm_sframes_t frames;
 
    for (i = 0; i < 4096; i++) {
        if (i % 64) {
          buffer[i] = 0xdd;
        } else {
          buffer[i] = 0xbb;
        }
    }
 
    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }
    if ((err = snd_pcm_set_params(handle,
                      SND_PCM_FORMAT_U8,
                      SND_PCM_ACCESS_RW_INTERLEAVED,
                      1,
                      48000,
                      1,
                      500000)) < 0) {   /* 0.5sec */
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }
 
        for (i = 0; i < 16; i++) {
        frames = snd_pcm_writei(handle, buffer, sizeof(buffer));
        if (frames < 0)
            frames = snd_pcm_recover(handle, frames, 0);
        if (frames < 0) {
            printf("snd_pcm_writei failed: %s\n", snd_strerror(frames));
            break;
        }
        if (frames > 0 && frames < (long)sizeof(buffer))
            printf("Short write (expected %li, wrote %li)\n", (long)sizeof(buffer), frames);
    }
 
    /* pass the remaining samples, otherwise they're dropped in close */
    err = snd_pcm_drain(handle);
    if (err < 0)
        printf("snd_pcm_drain failed: %s\n", snd_strerror(err));
    snd_pcm_close(handle);
    return 0;
}
```

***

#### TODO: pulseaudio simple API

https://www.freedesktop.org/software/pulseaudio/doxygen/simple.html

***

#### TODO: playing an audio file with pulse audio

According to chatgpt I should play an audio file with pulseaudio like this:
``` 
#include <stdio.h>
#include <unistd.h>
#include <pulse/simple.h>
#include <pulse/error.h>

#define BUFSIZE 1024

int main(int argc, char*argv[]) {

    /* The sample type to use */
    static const pa_sample_spec ss = {
        .format = PA_SAMPLE_S16LE,
        .rate = 44100,
        .channels = 2
    };

    pa_simple *s = NULL;
    int error;

    /* Open the connection to the server */
    s = pa_simple_new(NULL, "playback", PA_STREAM_PLAYBACK, NULL, "playback", &ss, NULL, NULL, &error);
    if (!s) {
        fprintf(stderr, "pa_simple_new() failed: %s\n", pa_strerror(error));
        return 1;
    }

    /* Open the audio file */
    FILE *f = fopen(argv[1], "rb");
    if (!f) {
        fprintf(stderr, "Could not open file '%s'\n", argv[1]);
        return 1;
    }

    /* Read data from the file and play it */
    uint8_t buf[BUFSIZE];
    size_t bytes_read;
    while ((bytes_read = fread(buf, 1, BUFSIZE, f)) > 0) {
        if (pa_simple_write(s, buf, bytes_read, &error) < 0) {
            fprintf(stderr, "pa_simple_write() failed: %s\n", pa_strerror(error));
            return 1;
        }
    }

    /* Make sure that everything has been played before we exit */
    if (pa_simple_drain(s, &error) < 0) {
        fprintf(stderr, "pa_simple_drain() failed: %s\n", pa_strerror(error));
        return 1;
    }

    /* Clean up */
    fclose(f);
    pa_simple_free(s);

    return 0;
}
``` 
This has better performance than doing a `system(paplay bla.mp3)` and you dont open up a new shell.

But you would have to install a pulseaudio dev package and then link to it...

***
