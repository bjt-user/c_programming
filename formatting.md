## indent

```
indent -linux main.c
```

```
indent -linux -brf my_file.c
```

`-brf` - Put braces on function definition line.


`-pal` - pointer aligned left (`char* buffer` instead of `char *buffer`):
```
indent -linux -brf -pal main.c
```

But if you do it like above `gnu indent` will always make backup files with `~` extension.\
To prevent this you can have to specify the output file: (you can use the input file as output file)
```
indent -linux -brf -pal main.c -o main.c
```

#### use tabs

`indent` theoretically uses tabs by default,\
but you need to set the width of the tab with `-i8` to make it work.\
`-i4` or smaller will use spaces.
