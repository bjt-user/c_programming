Problem: dealing with strings of unknown size

#### modifying a char pointer

It seems you need `malloc` to be able to modify char pointers.\
When you write `char *my_string = "test";` then this is a read-only string literal.\
So you need to allocate memory and then copy that string into it.

ChatGPT:
> String literals in C are special constants that represent arrays of characters. They are stored in a read-only section of memory, and their memory allocation and deallocation are handled by the compiler and the operating system.
> If you need to modify the string, you should use dynamic memory allocation to create a writable copy of the string

```
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
    const char *original_string = "hello";
    char *my_string = malloc(strlen(original_string) + 1);

    if (my_string != NULL) {
        strcpy(my_string, original_string);

        printf("Original: %s\n", my_string);

        my_string[0] = 't'; // Modify the first character

        printf("Modified: %s\n", my_string);

        free(my_string);
    } else {
        printf("Memory allocation failed.\n");
    }

    return 0;
}
```
You might do a memset to `\0` all characters of the string before copying to be safe.

#### sizeof a malloced char pointer

The `sizeof` a char pointer that was malloced with 256 bytes is just 8:
```
#include <stdio.h>
#include <stdlib.h>

int main() {
	char *my_stuff = malloc(256);
	printf("%ld\n", sizeof(my_stuff)); //8

	return 0;
}
```

So you probably dont have to zero out (`\0`) the malloced string with null terminators because memory is "dynamic".

#### you may access a char pointer by array indexes

```
#include <stdio.h>

int main() {
	char *my_ptr = "hello";

	printf("%c\n", my_ptr[2]);

	return 0;
}
```

#### Copying a char pointer into a char array by determining the string lenth with strlen

```
#include <stdio.h>
#include <string.h>

int main() {
	char *unknown_string = "hope you have cookies enabled.";
	size_t unknown_length = strlen(unknown_string);

	char array_string[unknown_length+1];
	memset(array_string, '\0', sizeof(array_string));
	strcpy(array_string, unknown_string);

	printf("%s\n", array_string);
	printf("%ld\n", unknown_length);
	printf("%ld\n", sizeof(array_string));
	printf("%ld\n", strlen(array_string));

	return 0;
}
```

But the problem is that you can't return arrays from functions in C.\
So you can't turn this into a function.

But you could modify a char pointer that way.\
You pass the char pointer to a function, you create a local array of strlen+1 inside the function.\
Modify the char array. Then return the first array element as a char pointer.

#### allocating memory inside function

When you allocate memory to a char pointer inside a function the caller of the function is responsible for freeing the memory.

An example would be if you return a char pointer that you did some modifications after declaring it.

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *get_string() {
        char *my_string = malloc(256);
        strcpy(my_string, "hello");
        return my_string;
}


int main() {
        char *new_string = get_string();

        printf ("%s\n", new_string);

        free(new_string);

        return 0;
}
```

**Important: Once you have malloced a char pointer, never assign a string literal to it, that will make the original allocation reference get lost and definatelly cause a memory leak. You have to use function like `strcpy`!**

If you have this line:
```
my_string = "hello";
```
instead of
```
strcpy(my_string, "hello");
```
you will have a memory leak and cant free the memory anymore.
