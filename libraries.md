#### gcc link libraries

To get a clue what you need to link you can try to search the libraries like this:
```
$ ls -la /usr/lib/x86_64-linux-gnu/ | grep -i "libpulse"
lrwxrwxrwx   1 root root        27 Oct 16  2022 libpulse-mainloop-glib.so -> libpulse-mainloop-glib.so.0
lrwxrwxrwx   1 root root        31 Oct 16  2022 libpulse-mainloop-glib.so.0 -> libpulse-mainloop-glib.so.0.0.6
-rw-r--r--   1 root root     18424 Oct 16  2022 libpulse-mainloop-glib.so.0.0.6
lrwxrwxrwx   1 root root        20 Oct 16  2022 libpulse-simple.so -> libpulse-simple.so.0
lrwxrwxrwx   1 root root        24 Oct 16  2022 libpulse-simple.so.0 -> libpulse-simple.so.0.1.1
-rw-r--r--   1 root root     22600 Oct 16  2022 libpulse-simple.so.0.1.1
lrwxrwxrwx   1 root root        13 Oct 16  2022 libpulse.so -> libpulse.so.0
lrwxrwxrwx   1 root root        18 Oct 16  2022 libpulse.so.0 -> libpulse.so.0.24.2
-rw-r--r--   1 root root    338992 Oct 16  2022 libpulse.so.0.24.2
```

So gcc options `-lpulse` and `-lpulse-simple` worked.

But it seems like you have to specify the libraries after your source files in the command:
```
gcc -Wall main.c -lpulse -lpulse-simple -lm
```

***
