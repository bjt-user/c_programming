To print integers:
```
printf ("%d", myint);
```

To print hexadecimal:
```
printf ("%x\n", mynumptr);
```

to print chararrays (which are called strings in other languages):
```
printf ("%s", mystring);
```

to print characters:
```
printf ("%c", mychar);
```

for pointers it seems to be:
```
printf ("%p", &myint);
```

#### clear the current line

Note that the printf does not contain a new line character in this example:
```
#include <stdio.h>
#include <unistd.h>

int main()
{
  for (int i = 0; i < 20; i++) {
    printf("%d", i);
    fflush(stdout);
    sleep(1);
    printf("\r");
  }
  return 0;
}
```

#### clear multiple lines

Here is a function you can use to clear multiple lines that have already been printed in the console.\
Those lines have been terminated with a `\n`.

```
#include <stdio.h>
#include <unistd.h>

void clear_lines(int);

int main() {
  printf("test\n");
  printf("test\n");
  printf("test\n");
  sleep(1);

  clear_lines(3);

  return 0;
}

void clear_lines(int lines_to_clear) {
  for (int i = 0; i < lines_to_clear; i++) {
      printf("\033[2K"); // clear the current line
      printf("\033[A"); // move the cursor up one line
  }
  printf("\033[2K"); // clear the last line
}
```
***

## troubleshooting

Sometimes printf doesnt print anything.
```
printf ("%d", countdown_sec);
```
> Output is buffered.
stdout is line-buffered by default, which means that '\n' is supposed to flush the buffer.

=> so can either add a `\n` to your printf or add a 
```
fflush(stdout);
```
to make the buffered stuff appear on the screen.

***
