```
man 2 stat
```

to get information about the struct:
```
man 3type stat
```

#### get the size of a file

```
#include <stdio.h>
#include <sys/stat.h>

int main() {
  char file_name[] = "the_c_book.pdf";

  struct stat my_stat;

  stat(file_name, &my_stat);

  printf ("%ld\n", my_stat.st_size);

  return 0;
}
```

#### get size of file function

Simple function without checking errors/return codes:
```
#include <stdio.h>
#include <sys/stat.h>

long int get_file_size(char file_name[]) {
  struct stat my_stat;
  stat(file_name, &my_stat);
  return my_stat.st_size;
}

int main() {
  char my_fn[] = "the_c_book.pdf";

  long int my_size = get_file_size(my_fn);

  printf ("The size of the ebook file is: %ld\n", my_size);

  return 0;
}
```
