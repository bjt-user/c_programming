Gets the current position of a `FILE`.

I wrote this function to quickly print the position of the opened file that is provided:
```
void print_file_position(FILE *open_file) {
  long my_position = 0;
  my_position = ftell(open_file);
  printf ("The position of the file is at: %ld\n", my_position);
}
```

#### get the number of characters of a file

Just `fseek` to the EOF, then use `ftell`.\
And you will get the same result as a `wc -c myfile`.\
This program executed in 0m0.002s according to time.\

```
#include <stdio.h>

int main(int argc, char** argv) {
        if (argc < 2) {
                printf ("Please provide a file as argument one.\n");
                return 1;
        }

        char* file_name = argv[1];

        FILE* my_file = fopen(file_name, "r");

        long length = 0;

        fseek(my_file, 0, SEEK_END);

        length = ftell(my_file);

        printf ("file_name: %s\n", file_name);
        printf ("length: %ld\n", length);

        return 0;
}
```

This way you know how much to malloc to read the entire file into a buffer.\
Recommended here:\
https://stackoverflow.com/questions/174531/how-to-read-the-content-of-a-file-to-a-string-in-c
