You can execute bash commands with the `popen()` function.

```
#include <stdio.h>

int main() {
    FILE* fp = popen("lsusb | grep -i 'logitech'", "r");
    if (fp == NULL) {
        perror("popen");
        return 1;
    }

    char buf[1024];
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        printf("%s", buf);
    }

    pclose(fp);
    return 0;
}
```

There is also `system()`.
That will also execute a bash command.\
You probably use that if you don't want to reuse the output of a bash command.
You need to `#include <stdlib.h>` for `system()`.
