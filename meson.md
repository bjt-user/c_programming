#### installation

```
sudo pacman -S meson
```

#### first example

`meson.build`:
```
project('tutorial', 'c')
executable('demo', 'main.c', 'helper.c')
```

`main.c`:
```
#include <stdio.h>
#include <stdlib.h>
#include "helper.h"

int main() {
	printf("main start\n");

	int mynum = 4;

	printf("%d\n", mynum);

	subtract_by_two(&mynum);

	printf("%d\n", mynum);

	return 0;
}
```

`helper.c`:
```
void subtract_by_two(int* num) {
	*num = *num - 2;
}
```

`helper.h`:
```
int subtract_by_two(int* num);
```

1. `meson setup builddir`
1. `cd builddir`
1. `meson compile`
1. `./demo`

#### unit testing

https://mesonbuild.com/Unit-tests.html

You have to compile test executables.

`meson.build`:
```
project('tutorial', 'c')
executable('demo', 'main.c', 'helper.c')

e = executable('testhelper', 'test_helper.c', 'helper.c')
test('test subtract_by_two', e)
```
(You need to compile the test executable with the c file you want to test.)

This is the test file `test_helper.c`:
```
#include <assert.h>
#include "helper.h"

int main() {
	int mynum = 44;
	subtract_by_two(&mynum);
	assert(mynum == 43);

    return 0;
}
```

Now you can go into `builddir` and do `meson compile`.\
Then run `testhelper`.

Or run
```
meson test
```


You can also use a return value other than 0 to indicate failure in your `test_helper.c`:
```
#include <assert.h>
#include "helper.h"

int main() {
	int mynum = 44;
	subtract_by_two(&mynum);

	if (mynum != 42) {
		return 1;
	}

	return 0;
}
```

#### linking external libraries

If you want to link libs like "uuid" when comiling: (like passing `-luuid` to `gcc`)
```
executable('myprogram', 'main.c', 'cli_arg_parsing.c', link_args : '-luuid')
```

#### debug test executables

The test executables are compiled with debug flags by default.

You can just run `gdb testexecutable` to debug a test.

#### install your project

https://mesonbuild.com/Installing.html

Just add the `install` and `install_dir` arguments to the executable function:
```
executable('myprogram', 'main.c', 'cli_arg_parsing.c', install: true, install_dir: '/usr/local/bin')
```

Then execute this command in the `builddir` dir:
```
meson install
```

According to the docs you should also be able to install additional files like manpages or libraries.


#### change dir before executing meson (`-C`)

```
meson test -C testdir/
```
