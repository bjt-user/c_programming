`man 3 getenv`

> SYNOPSIS
```
#include <stdlib.h>

char *getenv(const char *name);
```
> The getenv() function returns a pointer to the value in the environment, or NULL if there is no match.

**Do not use the return value directly! strcpy it into another variable!**

#### WARNING: be careful in combination with strcat

Since `getenv` returns a pointer to the actual location of the environment variable in memory \
and `strcat` changes the argument that you pass it to, using these two function in combination \
might change the environment variable inside your program.

This actually changed the environment variable `$HOME` inside my program:
```
char *HOME = getenv("HOME");
char *audio_file = "";
audio_file = strcat(HOME, "/music/alarm-clock-elapsed.wav");
```
When you do another `getenv("HOME")` afther that it will actually return `/home/user/music/alarm-clock-elapsed.wav`.\
After the program exits, `$HOME` is back to normal because of automatic deallocation.

#### using strcpy to copy environment variable into a char array
This gets the HOME env var, copies it into char array then appends a relative path to it.
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char *my_home = getenv("HOME");

	printf ("%s\n", my_home);

	char my_path[256] = "";
	strcpy(my_path, my_home);

	printf("my_home: %s\n", my_home);
	printf("address of my_home: %p\n", my_home);
	printf("my_path: %s\n", my_path);
	printf("address of my_path: %p\n", my_path);

	char relative_path[256] = "/test/myfile.txt";

	strcat(my_path, relative_path);
	printf("my_path: %s\n", my_path);

	return 0;
}
```

#### use strdup to create a copy of the environment variable string

Another approach if you like working with char pointers:
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char *my_home = getenv("HOME");

	printf ("address of my_home: %p\n", my_home);

	char *home_copy = strdup(my_home);

	printf ("home_copy: %s\n", home_copy);
	printf ("address of home_copy: %p\n", home_copy);

	free(home_copy);

	return 0;
}
```

```
address of my_home: 0x7fffee0ab98d
home_copy: /home/user
address of home_copy: 0x563ce8a576b0
```

Remember to free the memory allocated by `strdup`, by doing a free on the char pointer it returned.
